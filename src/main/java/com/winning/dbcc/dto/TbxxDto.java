package com.winning.dbcc.dto;

/**
 * 投保信息
 * @author tech-winning
 *
 */

public class TbxxDto {
	private String xh;				//
	private String nf;				//
	private String tblx;			//
	private String insu_id;			//
	private String insu_nm;			//
	private String csrq;			//
	private String sfzh;			//
	private String sex;				//
	private String sftb;			//
	private String tbsd;			//
	private String jzd;				//
	private String tsmz;			//
	private String lbzh;			//
	private String qsgx;			//
	private String mzjbbe;			//
	private String zyjbbe;			//
	private String ygxm;			//
	private String ygbm;			//
	private String dwmc;			//
	private String mzsypf;			//
	private String zysypf;			//
	private String ywshjzbe;		//
	private String ksrq;			//
	private String ywshsypfje;		//
	private String ywylsypfje;		//
	private String ywyljzbe;		//
	private String ywshbxjzje;
	private String jsrq;			//
	private String fsftb;			//
	private String fygxm;			//
	private String msftb;			//
	private String mygxm;			//
	private String fdwmc;			//
	private String mdwmc;			//
	private String fygbm;			//
	private String mygbm;			//
	private String dftblx;			//
	
	
	
	
	
	public String getXh() {
		return xh;
	}
	public void setXh(String xh) {
		this.xh = xh;
	}
	public String getNf() {
		return nf;
	}
	public String getTblx() {
		return tblx;
	}
	public String getInsu_id() {
		return insu_id;
	}
	public String getInsu_nm() {
		return insu_nm;
	}
	public String getCsrq() {
		return csrq;
	}
	public String getSfzh() {
		return sfzh;
	}
	public String getSex() {
		return sex;
	}
	public String getSftb() {
		return sftb;
	}
	public String getTbsd() {
		return tbsd;
	}
	public String getJzd() {
		return jzd;
	}
	public String getTsmz() {
		return tsmz;
	}
	public String getLbzh() {
		return lbzh;
	}
	public String getQsgx() {
		return qsgx;
	}
	public String getMzjbbe() {
		return mzjbbe;
	}
	public String getZyjbbe() {
		return zyjbbe;
	}
	public String getYgxm() {
		return ygxm;
	}
	public String getYgbm() {
		return ygbm;
	}
	public String getDwmc() {
		return dwmc;
	}
	public String getMzsypf() {
		return mzsypf;
	}
	public String getZysypf() {
		return zysypf;
	}
	public String getYwshjzbe() {
		return ywshjzbe;
	}
	public String getKsrq() {
		return ksrq;
	}
	public String getYwshsypfje() {
		return ywshsypfje;
	}
	public String getYwylsypfje() {
		return ywylsypfje;
	}
	public String getYwyljzbe() {
		return ywyljzbe;
	}
	public String getJsrq() {
		return jsrq;
	}
	public void setNf(String nf) {
		this.nf = nf;
	}
	public void setTblx(String tblx) {
		this.tblx = tblx;
	}
	public void setInsu_id(String insu_id) {
		this.insu_id = insu_id;
	}
	public void setInsu_nm(String insu_nm) {
		this.insu_nm = insu_nm;
	}
	public void setCsrq(String csrq) {
		this.csrq = csrq;
	}
	public void setSfzh(String sfzh) {
		this.sfzh = sfzh;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public void setSftb(String sftb) {
		this.sftb = sftb;
	}
	public void setTbsd(String tbsd) {
		this.tbsd = tbsd;
	}
	public void setJzd(String jzd) {
		this.jzd = jzd;
	}
	public void setTsmz(String tsmz) {
		this.tsmz = tsmz;
	}
	public void setLbzh(String lbzh) {
		this.lbzh = lbzh;
	}
	public void setQsgx(String qsgx) {
		this.qsgx = qsgx;
	}
	public void setMzjbbe(String mzjbbe) {
		this.mzjbbe = mzjbbe;
	}
	public void setZyjbbe(String zyjbbe) {
		this.zyjbbe = zyjbbe;
	}
	public void setYgxm(String ygxm) {
		this.ygxm = ygxm;
	}
	public void setYgbm(String ygbm) {
		this.ygbm = ygbm;
	}
	public void setDwmc(String dwmc) {
		this.dwmc = dwmc;
	}
	public void setMzsypf(String mzsypf) {
		this.mzsypf = mzsypf;
	}
	public void setZysypf(String zysypf) {
		this.zysypf = zysypf;
	}
	public void setYwshjzbe(String ywshjzbe) {
		this.ywshjzbe = ywshjzbe;
	}
	public void setKsrq(String ksrq) {
		this.ksrq = ksrq;
	}
	public void setYwshsypfje(String ywshsypfje) {
		this.ywshsypfje = ywshsypfje;
	}
	public void setYwylsypfje(String ywylsypfje) {
		this.ywylsypfje = ywylsypfje;
	}
	public void setYwyljzbe(String ywyljzbe) {
		this.ywyljzbe = ywyljzbe;
	}
	public void setJsrq(String jsrq) {
		this.jsrq = jsrq;
	}
	public String getFsftb() {
		return fsftb;
	}
	public String getFygxm() {
		return fygxm;
	}
	public String getMsftb() {
		return msftb;
	}
	public String getMygxm() {
		return mygxm;
	}
	public String getFdwmc() {
		return fdwmc;
	}
	public String getMdwmc() {
		return mdwmc;
	}
	public String getFygbm() {
		return fygbm;
	}
	public String getMygbm() {
		return mygbm;
	}
	public void setFsftb(String fsftb) {
		this.fsftb = fsftb;
	}
	public void setFygxm(String fygxm) {
		this.fygxm = fygxm;
	}
	public void setMsftb(String msftb) {
		this.msftb = msftb;
	}
	public void setMygxm(String mygxm) {
		this.mygxm = mygxm;
	}
	public void setFdwmc(String fdwmc) {
		this.fdwmc = fdwmc;
	}
	public void setMdwmc(String mdwmc) {
		this.mdwmc = mdwmc;
	}
	public void setFygbm(String fygbm) {
		this.fygbm = fygbm;
	}
	public void setMygbm(String mygbm) {
		this.mygbm = mygbm;
	}
	public String getYwshbxjzje() {
		return ywshbxjzje;
	}
	public void setYwshbxjzje(String ywshbxjzje) {
		this.ywshbxjzje = ywshbxjzje;
	}
	public String getDftblx() {
		return dftblx;
	}
	public void setDftblx(String dftblx) {
		this.dftblx = dftblx;
	}
	
	
	
	
	
	
	

}
