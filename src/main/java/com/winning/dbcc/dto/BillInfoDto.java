package com.winning.dbcc.dto;

public class BillInfoDto {
    
    private String caseId;
    
    private String billCode;
    
    private Long billId;
    
    private String clinDate;
    
    private String hosptailName;
    
    private Double totalCost;
    
    private Double thirdPay;
    
    private Double cutPay;
    
    private String clinType; //门诊、住院
    
    private String diagnosis; //主诊断
    
    private Double compPay;
    
    private String medType;
    
    private String resudEsc;

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }
    

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getClinDate() {
        return clinDate;
    }

    public void setClinDate(String clinDate) {
        this.clinDate = clinDate;
    }

    public String getHosptailName() {
        return hosptailName;
    }

    public void setHosptailName(String hosptailName) {
        this.hosptailName = hosptailName;
    }


    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public Double getThirdPay() {
        return thirdPay;
    }

    public void setThirdPay(Double thirdPay) {
        this.thirdPay = thirdPay;
    }

    public Double getCutPay() {
        return cutPay;
    }

    public void setCutPay(Double cutPay) {
        this.cutPay = cutPay;
    }

    public String getClinType() {
        return clinType;
    }

    public void setClinType(String clinType) {
        this.clinType = clinType;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public Double getCompPay() {
        return compPay;
    }

    public void setCompPay(Double compPay) {
        this.compPay = compPay;
    }

    public String getMedType() {
        return medType;
    }

    public void setMedType(String medType) {
        this.medType = medType;
    }

	public String getResudEsc() {
		return resudEsc;
	}

	public void setResudEsc(String resudEsc) {
		this.resudEsc = resudEsc;
	}
    
}
