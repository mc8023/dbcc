package com.winning.dbcc.dto;

public class IllegalInfoDto {
    
    private String billId;
    
    private String billCode;
    
    private String xmmc;
    
    /**违规类型*/
    private String type;
    
    private String deductionReason;
    
    private Double cutAmount;
    
    private String clinDate;
    
    private String hosptailName;

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }
    

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getXmmc() {
        return xmmc;
    }

    public void setXmmc(String xmmc) {
        this.xmmc = xmmc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDeductionReason() {
        return deductionReason;
    }

    public void setDeductionReason(String deductionReason) {
        this.deductionReason = deductionReason;
    }

    public Double getCutAmount() {
        return cutAmount;
    }

    public void setCutAmount(Double cutAmount) {
        this.cutAmount = cutAmount;
    }

    public String getClinDate() {
        return clinDate;
    }

    public void setClinDate(String clinDate) {
        this.clinDate = clinDate;
    }

    public String getHosptailName() {
        return hosptailName;
    }

    public void setHosptailName(String hosptailName) {
        this.hosptailName = hosptailName;
    }

    

}
