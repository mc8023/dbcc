package com.winning.dbcc.dto;

/**
 * 
 * <p>@describe: 理赔月报表 查询参数 </P>
 * <p>@title: LiPeiYueBaoBiaoDto.java </P>
 * <p>@package: com.winning.dbcc.dto </P>
 * <p>@date: 2017年5月19日 下午6:48:21  </P>
 * <p>@author: wangaogao  </P> 
 * <p>@Copyright: (c) 2017, 上海金仕达卫宁软件科技有限公司  </P>
 */
public class LiPeiYueBaoBiaoDto {
	/**开始日期*/
	private String beginClinDate;
	
	/**结束日期*/
	private String endClinDate;
	
	/**板块*/
	private String bk;
	
	private String lplx;

	public String getBeginClinDate() {
		return beginClinDate;
	}

	public void setBeginClinDate(String beginClinDate) {
		this.beginClinDate = beginClinDate;
	}

	public String getEndClinDate() {
		return endClinDate;
	}

	public void setEndClinDate(String endClinDate) {
		this.endClinDate = endClinDate;
	}

	public String getBk() {
		return bk;
	}

	public void setBk(String bk) {
		this.bk = bk;
	}

	public String getLplx() {
		return lplx;
	}

	public void setLplx(String lplx) {
		this.lplx = lplx;
	}
	
}
