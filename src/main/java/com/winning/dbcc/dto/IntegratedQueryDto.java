package com.winning.dbcc.dto;

public class IntegratedQueryDto {
    
    private String caseId;// 案件号
    
    private String tbrxm;// 投保人姓名
    
    private String bbrxm;// 被保人姓名
    
    private String bbrsfz;// 被保人身份证
    
    private String ygbh;// 员工编号
    
    private String dwmc;// 单位名称
    
    private String jzyy;// 就诊医院
    
    private String jzrqf;// 就诊开始时间
    
    private String jzrqt;// 就诊结束时间
    
    private String jsrqf;// 结算开始时间
    
    private String jsrqt;// 结算结束时间
    
    private String nd;// 年度
    
    private String dcInfo;// 导出详细信息
    
    private String id;
    
    private String name;
    
	private String person_level;//查询级别
	
	private String query_condition;//查询条件
	
	private String jmhs;
	
	private String zhcx;
	
	private long userid;
    

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public String getTbrxm() {
		return tbrxm;
	}

	public void setTbrxm(String tbrxm) {
		this.tbrxm = tbrxm;
	}

	public String getBbrxm() {
		return bbrxm;
	}

	public void setBbrxm(String bbrxm) {
		this.bbrxm = bbrxm;
	}

	public String getBbrsfz() {
		return bbrsfz;
	}

	public void setBbrsfz(String bbrsfz) {
		this.bbrsfz = bbrsfz;
	}

	public String getYgbh() {
		return ygbh;
	}

	public void setYgbh(String ygbh) {
		this.ygbh = ygbh;
	}

	public String getDwmc() {
		return dwmc;
	}

	public void setDwmc(String dwmc) {
		this.dwmc = dwmc;
	}

	public String getJzyy() {
		return jzyy;
	}

	public void setJzyy(String jzyy) {
		this.jzyy = jzyy;
	}

	public String getJzrqf() {
		return jzrqf;
	}

	public void setJzrqf(String jzrqf) {
		this.jzrqf = jzrqf;
	}

	public String getJzrqt() {
		return jzrqt;
	}

	public void setJzrqt(String jzrqt) {
		this.jzrqt = jzrqt;
	}

	public String getJsrqf() {
		return jsrqf;
	}

	public void setJsrqf(String jsrqf) {
		this.jsrqf = jsrqf;
	}

	public String getJsrqt() {
		return jsrqt;
	}

	public void setJsrqt(String jsrqt) {
		this.jsrqt = jsrqt;
	}

	public String getNd() {
		return nd;
	}

	public void setNd(String nd) {
		this.nd = nd;
	}

	public String getDcInfo() {
		return dcInfo;
	}

	public void setDcInfo(String dcInfo) {
		this.dcInfo = dcInfo;
	}

	public String getPerson_level() {
		return person_level;
	}

	public void setPerson_level(String person_level) {
		this.person_level = person_level;
	}

	public String getQuery_condition() {
		return query_condition;
	}

	public void setQuery_condition(String query_condition) {
		this.query_condition = query_condition;
	}

	public String getJmhs() {
		return jmhs;
	}

	public void setJmhs(String jmhs) {
		this.jmhs = jmhs;
	}

	public String getZhcx() {
		return zhcx;
	}

	public void setZhcx(String zhcx) {
		this.zhcx = zhcx;
	}

	public long getUserid() {
		return userid;
	}

	public void setUserid(long userid) {
		this.userid = userid;
	}
    

}
