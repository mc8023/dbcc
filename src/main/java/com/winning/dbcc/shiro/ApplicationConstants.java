package com.winning.dbcc.shiro;

/**
 * 项目静态常量类
 */
public class ApplicationConstants {

    //保存session中存放用户信息的键
    public static final String LOGIN_USER = "loginUser";
}
