package com.winning.dbcc.service.inter;

import java.io.Serializable;
import java.util.List;

import com.github.pagehelper.Page;
import com.winning.dbcc.comm.QueryParamBean;
import com.winning.dbcc.comm.bean.PageParamBean;
/**
 * 通用Service
 * @author Administrator
 *
 * @param <T>
 * @param <ID>
 */
public interface BaseService<T,ID extends Serializable> {
	
	public int add(T record);
	
	public int deleteById(ID id);
	
	public int deleteByIds(Integer[] ids);
	
	public int updateBySelective(T record);
	
	public T findById(ID id);
	
	public Page<T> findPageList(PageParamBean pageParam);
	
	public Page<T> findPageListBySelective(PageParamBean pageParam,T record);
	
	public Page<T> findPageListByParamBean(PageParamBean pageParam,QueryParamBean bean);
	
	public List<T> findAllList();
	
	public List<T> findListBySelective(T record);
	
	public int addBatch(List<T> records);
}
