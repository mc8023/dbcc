package com.winning.dbcc.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.winning.dbcc.comm.QueryParamBean;
import com.winning.dbcc.comm.bean.PageParamBean;
import com.winning.dbcc.enter.SysMenu;
import com.winning.dbcc.mapper.BaseMapper;
import com.winning.dbcc.mapper.HomePageMapper;

@Service
public class HomePageServiceImpl extends BaseServiceImpl<SysMenu, Integer> {

	@Resource
	private HomePageMapper homePageMapper;
	
	public BaseMapper getMapper(){
		return homePageMapper;
	}
	
	public List<SysMenu> findSysMenuByUserId(Integer userId){
	    return homePageMapper.findSysMenuByUserId(userId);
	}

	@Override
	public Page<SysMenu> findPageList(PageParamBean pageParam) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<SysMenu> findPageListBySelective(PageParamBean pageParam, SysMenu record) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<SysMenu> findPageListByParamBean(PageParamBean pageParam, QueryParamBean bean) {
		// TODO Auto-generated method stub
		return null;
	}

}
