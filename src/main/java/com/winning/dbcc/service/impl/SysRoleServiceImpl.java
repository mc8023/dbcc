package com.winning.dbcc.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.winning.dbcc.enter.SysRole;
import com.winning.dbcc.mapper.BaseMapper;
import com.winning.dbcc.mapper.SysRoleMapper;

@Service
public class SysRoleServiceImpl extends BaseServiceImpl<SysRole, Integer> {

	@Resource
	private SysRoleMapper sysRoleMapper;
	
	@Override
	public BaseMapper getMapper(){
		return sysRoleMapper;
	}

}
