package com.winning.dbcc.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.winning.dbcc.comm.Constant;
import com.winning.dbcc.enter.SysUserRole;
import com.winning.dbcc.mapper.BaseMapper;
import com.winning.dbcc.mapper.SysUserRoleMapper;

@Service
public class SysUserRoleServiceImpl extends BaseServiceImpl<SysUserRole, Integer>{

    @Resource
    private SysUserRoleMapper sysUserRoleMapper;
    
    @Override
    public BaseMapper getMapper() {
        return sysUserRoleMapper;
    }
    
    public List<SysUserRole> getUserRolesByUserId(Integer userId){
        return sysUserRoleMapper.getUserRolesByUserId(userId);
    }
    
 	public void addBatchUserRole(Integer[] ids, Integer userId) {
		List<SysUserRole> sruList = new ArrayList<SysUserRole>();
		for (int i = 0; i < ids.length; i++) {
			SysUserRole userRole = new SysUserRole();
			userRole.setRoleId(ids[i]);
			userRole.setUserId(userId);
			userRole.setStatus(Constant.STATUS_EFFECTIVE);
			sruList.add(userRole);
		}
		sysUserRoleMapper.deleteByUserId(userId);
		sysUserRoleMapper.addBatch(sruList);
	}
    
    

}
