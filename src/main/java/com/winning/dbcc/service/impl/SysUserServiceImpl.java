package com.winning.dbcc.service.impl;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.winning.dbcc.mapper.BaseMapper;
import com.winning.dbcc.mapper.SysUserMapper;
import com.winning.dbcc.dto.Edtpsw;
import com.winning.dbcc.enter.SysUser;

@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUser, Integer> {

	@Resource
	private SysUserMapper sysUserMapper;
	
	public BaseMapper getMapper(){
		return sysUserMapper;
	}

	public boolean usernameIsExists(String username) {
		if(StringUtils.isNotBlank(sysUserMapper.usernameIsExists(username))){
			return true;
		}
		return false;
	}
	
	public SysUser getSysUserByUsername(String username){
	    
	    return sysUserMapper.getSysUserByUsername(username);
	}

	public void updateuserpsw(Edtpsw psw) {
		 sysUserMapper.updateuserpsw(psw);
	}

	public Edtpsw findpzmm() {
		return sysUserMapper.findpzmm();
	}


}
