package com.winning.dbcc.enter;

import java.util.List;

import org.apache.commons.lang.StringUtils;

public class SysMenu {
	
	private Integer id;
	
	private String name; 
	
	private String url;
	
	private Integer seq;
	
	private Integer parent;
	
	private Integer status;
	
	private Integer menu_level;
	
	private String description;
	
	private String icon;
	
	/**
	 * 子节点
	 */
	private List<SysMenu> childSysFuncs;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Integer getParent() {
		return parent;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	

	public Integer getMenu_level() {
		return menu_level;
	}

	public void setMenu_level(Integer menu_level) {
		this.menu_level = menu_level;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

    public List<SysMenu> getChildSysFuncs() {
        return childSysFuncs;
    }

    public void setChildSysFuncs(List<SysMenu> childSysFuncs) {
        this.childSysFuncs = childSysFuncs;
    }
	
	private String checkstr;

	public String getCheckstr() {
		if(StringUtils.isNotBlank(checkstr)){
			return "checked";
		}else{
			return "";
		}
	}

	public void setCheckstr(String checkstr) {
		this.checkstr = checkstr;
	}
}
