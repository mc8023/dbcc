package com.winning.dbcc.enums;

/**
 * 系统角色表枚举
 * @author cuihom
 *
 */
public enum SysRoleType {
    NO_LOGIN("未登录", 0), ADMIN("管理员", 1),  EMPLOYEE("员工", 2);

    private String name;

    private Integer id;

    SysRoleType(String name, Integer id) {
        setName(name);
        setId(id);
    }

    public static String getName(Integer id) {
        for (SysRoleType c : SysRoleType.values()) {
            if (c.getId() == id) {
                return c.getName();
            }
        }
        return null;
    }

    public static SysRoleType get(Integer id) {
        for (SysRoleType c : SysRoleType.values()) {
            if (c.getId() == id) {
                return c;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


}
