package com.winning.dbcc.mapper;

import java.util.List;

import com.winning.dbcc.enter.SysUserRole;
import com.winning.dbcc.mapper.BaseMapper;

public interface SysUserRoleMapper extends BaseMapper<SysUserRole, Integer>{
    
    public List<SysUserRole> getUserRolesByUserId(Integer userId);
    
    void deleteByUserId(Integer userId);

	public void deletejgbyuserid(String id);

}
