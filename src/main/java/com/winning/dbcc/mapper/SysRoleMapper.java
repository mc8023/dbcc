package com.winning.dbcc.mapper;


import com.winning.dbcc.enter.SysRole;
import com.winning.dbcc.mapper.BaseMapper;

public interface SysRoleMapper extends BaseMapper<SysRole, Integer>{

}
