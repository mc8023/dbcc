package com.winning.dbcc.controller;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONException;
import com.github.pagehelper.Page;
import com.winning.dbcc.comm.AjaxMessage;
import com.winning.dbcc.comm.PageResult;
import com.winning.dbcc.comm.QueryParamBean;
import com.winning.dbcc.comm.bean.PageParamBean;
import com.winning.dbcc.dto.Edtpsw;
import com.winning.dbcc.dto.SettlementDetailQueryDto;
import com.winning.dbcc.enter.SysRole;
import com.winning.dbcc.enter.SysUser;
import com.winning.dbcc.enter.SysUserRole;
import com.winning.dbcc.service.impl.SysRoleServiceImpl;
import com.winning.dbcc.service.impl.SysUserRoleServiceImpl;
import com.winning.dbcc.service.impl.SysUserServiceImpl;
import com.winning.dbcc.shiro.ApplicationConstants;
import com.winning.dbcc.shiro.AuthorizingRealm;

/**
 * 系统管理Controller 
 * @author tiansheng
 */
@Controller
@RequestMapping("/sys")
public class SystemController{
	
	private static final Log logger = LogFactory.getLog(SystemController.class);
	
	@Resource
	SysUserServiceImpl sysUserServiceImpl;
	
	@Resource
	SysRoleServiceImpl sysRoleServiceImpl;
	
	@Resource
	SysUserRoleServiceImpl sysUserRoleServiceImpl;
	
	@Autowired
	private RealmSecurityManager securityManager;
	
	/********************************用户管理**************************************************/
	
	@RequestMapping("/user")
	public String index(HttpServletRequest request,String username_param){
		PageParamBean pageParam = new PageParamBean(request);
		QueryParamBean bean = new QueryParamBean();
		bean.setUsername(username_param);
		Page page =sysUserServiceImpl.findPageListByParamBean(pageParam, bean);
		request.setAttribute("pageResult", new PageResult(page));
		request.setAttribute("username_param", username_param);
		List<SysRole> roleList = sysRoleServiceImpl.findAllList();
		request.setAttribute("roleList", roleList);
		return "jsp/sys/user_list";
	}
	
	@RequestMapping(value="/checkUsernameIsExists",method=RequestMethod.GET)
	@ResponseBody
	public String checkUsernameIsExists(HttpServletRequest request,String username){
		if(sysUserServiceImpl.usernameIsExists(username)){
			return "true";
		}else{
			return "false";
		}
	}
	
	@RequestMapping("/user/delete/{id}")
	@ResponseBody
	public String deleteUser(@PathVariable Integer id) {
		sysUserServiceImpl.deleteById(id);
		Subject subject = SecurityUtils.getSubject();
        Session session = subject.getSession();
        // 把用户角色放进session
        SysUser user = (SysUser) session.getAttribute(ApplicationConstants.LOGIN_USER);
		return new AjaxMessage().setData(user).toJsonstring();
	}
	
	/********************************菜单管理**************************************************/

	
	/********************************角色管理**************************************************/
	
	
	@RequestMapping("/role/post")
	@ResponseBody
	public String saveRole(SysRole role) {
		SysRole orgin = sysRoleServiceImpl.findById(role.getId());
		if (orgin == null) {
			orgin = new SysRole();
			logger.info("create one SysRole !! " + role.getName());
		}
		BeanUtils.copyProperties(role, orgin);
		if(role.getName().trim().length()<=0){
			return new AjaxMessage().addError("角色名称为必填项!").toJsonstring();
		}
		/*if("-1".equals(role.getStatus())){
			return new AjaxMessage().addError("角色状态为必填项!").toJsonstring();
		}*/
		if(role.getId()!=null&&role.getId()>0){
			sysRoleServiceImpl.updateBySelective(orgin);
		}else{
			sysRoleServiceImpl.add(orgin);
		}
		role.setId(orgin.getId());
		return new AjaxMessage().setData(role).toJsonstring();
	}
	
	@RequestMapping("/role/get/{id}")
	@ResponseBody
	public String getRoleInfo(@PathVariable Integer id) {
		SysRole role = sysRoleServiceImpl.findById(id);
		return new AjaxMessage().setData(role).toJsonstring();
	}
	
	@RequestMapping("/role/delete/{id}")
	@ResponseBody
	public String deletRole(@PathVariable Integer id) {
		sysRoleServiceImpl.deleteById(id);
		return new AjaxMessage().setData("").toJsonstring();
	}
	
	/********************************分配角色**************************************************/
	@RequestMapping("/user_role/getByUserId/{id}")
	@ResponseBody
	public String getUserRoleInfo(@PathVariable Integer id) {
		SysUserRole record = new SysUserRole();
		record.setUserId(id);
		List<SysUserRole> userRoleList =sysUserRoleServiceImpl.findListBySelective(record); 
		return new AjaxMessage().setData(userRoleList).toJsonstring();
	}
	
	@RequestMapping(value="/user_role/post", method=RequestMethod.POST)
	@ResponseBody
	public String addBatchUserRole(Integer[] ids,Integer userId) {
		sysUserRoleServiceImpl.addBatchUserRole(ids,userId);
		return new AjaxMessage().setData("").toJsonstring();
	}
	
	
	   /**利用MD5进行加密
     * @param str  待加密的字符串
     * @return  加密后的字符串
     * @throws NoSuchAlgorithmException  没有这种产生消息摘要的算法
     * @throws UnsupportedEncodingException  
     */
    public String EncoderByMd5(String str){
		MessageDigest md;
	     StringBuffer sb = new StringBuffer();
	     try {
	          md = MessageDigest.getInstance("MD5");
	          md.update(str.getBytes());
	          byte[] data = md.digest();
	          int index;
	          for(byte b : data) {
	               index = b;
	               if(index < 0) index += 256;
	               if(index < 16) sb.append("0");
	               sb.append(Integer.toHexString(index));
	          }
	     } catch (NoSuchAlgorithmException e) {
	      e.printStackTrace();
	     }
	     System.out.println(sb.toString());
	     return sb.toString();
    }
    
    

    
    @RequestMapping(value="/user_role/keepjg", method=RequestMethod.POST)
	@ResponseBody
	public String keepjg(Integer[] ids,Integer userId) {
		sysUserRoleServiceImpl.addBatchUserRole(ids,userId);
		return new AjaxMessage().setData("").toJsonstring();
	}
    
	
	@RequestMapping("/editpsw")
	public String editpsw(HttpServletRequest request){
		PageParamBean pageParam = new PageParamBean(request);
		
		Subject subject = SecurityUtils.getSubject();
		Session session = subject.getSession();
		SysUser user = (SysUser) session.getAttribute(ApplicationConstants.LOGIN_USER);
		request.setAttribute("password", user.getPassword());
		return "jsp/sys/editpsw";
	}
    
	
	@RequestMapping("/user/pwd")
	@ResponseBody
	public String savepwd(String psw1,String psw2,String psw3) throws InterruptedException{
		
		Subject subject = SecurityUtils.getSubject();
		Session session = subject.getSession();
		SysUser user = (SysUser) session.getAttribute(ApplicationConstants.LOGIN_USER);
		Edtpsw psw = new Edtpsw();
		psw.setPsw1(psw1);
		psw.setPsw2(psw2);
		psw.setPsw3(psw3);
		psw.setId(user.getId());
		psw.setUsername(user.getUsername());
		
		sysUserServiceImpl.updateuserpsw(psw);
		
		securityManager =  
				(RealmSecurityManager) SecurityUtils.getSecurityManager();  
		AuthorizingRealm userRealm = (AuthorizingRealm) securityManager.getRealms().iterator().next();  
		userRealm.clearCachedAuthenticationInfo(subject.getPrincipals());  
		
//		TimeUnit.MILLISECONDS.sleep(10000);
		return new AjaxMessage().setData(psw).toJsonstring();
		}
	
	
	@RequestMapping("/user/restpsw")
	@ResponseBody
	public String restpsw(String username,Integer id){
		
		Edtpsw mm = sysUserServiceImpl.findpzmm();
		  
		Edtpsw psw = new Edtpsw();
		psw.setId(id);
		psw.setUsername(username);
		psw.setPsw2(EncoderByMd5(mm.getPsw2()));
		
		sysUserServiceImpl.updateuserpsw(psw);
		return new AjaxMessage().setData(psw).toJsonstring();
		}
}
