package com.winning.dbcc.comm;

import com.github.pagehelper.Page;

public class PageResult {
	
	private Page pageList;
	
	private int pages;
	
	private int pageSize;
	
	private long total;
	
	private int pageNum;
	
	public PageResult(Page page) {
		pageList = page;
		pages = page.getPages();
		pageSize = page.getPageSize();
		total = page.getTotal();
		pageNum = page.getPageNum();
	}

	public Page getPageList() {
		return pageList;
	}

	public void setPageList(Page pageList) {
		this.pageList = pageList;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

}
