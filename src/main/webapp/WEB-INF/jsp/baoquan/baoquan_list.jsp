<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@include file="/WEB-INF/comm/_env.jsp"%>
<title>东风 | 菜单管理</title>
</head>
<body>
			<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<ul class="breadcrumb">
									</ul>
									<form class="form-inline" role="form" action="${ROOT}/baoquan/baoquanList.html">
										<div class="form-group">
											<label class="sr-only"></label>
											<div class="chaxun_input_div">
											  <input type="text" class="form-control" id="chan_id" name="chan_id" placeholder="保全编号">
											</div> 
											
										</div>
										<div class="form-group">
											<label class="sr-only"></label> 
											<div class="chaxun_input_div">
												<input
													type="text" class="form-control" name="status"
													id=status placeholder="保全状态">
											</div> 
										</div>
										<div class="form-group">
											<label class="sr-only"></label> 
											<div class="chaxun_input_div">
												<input
													type="text" class="form-control" name="insuName"
													id="insuName" placeholder="被保险人姓名">
											</div> 
										</div>
										
										<div class="form-group">
											<label class="sr-only"></label> 
											<div class="chaxun_input_div">
												<input
													type="text" class="form-control" name="idCard"
													id="idCard" placeholder="被保险人证件号">
											</div> 
										</div>
										<div class="form-group">
											<label class="sr-only"></label> 
											<div class="chaxun_input_div">
												<button type="submit" class="btn btn-primary-cx">查询</button>
											</div> 
										</div>

									<!-- 	<div class="box">
											<div class="box-title">
												<div class="tools" style="margin-right: 47%">
													<a href="javascript:;" class="collapse"> <span>高级查询</span>
														<i class="fa fa-chevron-down"></i>
													</a>
												</div>
											</div>
											<div class="box-body font-400" style="display: none;">
											   <div class="form-group">
											        <label class="sr-only"></label> 
													<div class="chaxun_input_div">
														<input
															type="text" class="form-control" name="miCardId"
															id="miCardId" placeholder="可选投保年度.....">
													</div> 
											    </div>
												<div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_begin">
													    <input name="beginClinDate" id="beginClinDate" type="text" placeholder="就诊日期  自" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'endClinDate\')}'})"/>
										            </div> 
										        </div>
										        <div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_end">
													    <input name="endClinDate" id="endClinDate" type="text" placeholder="至" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'beginClinDate\')}'})"/>
										            </div> 
										        </div>
										        <div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_begin">
													    <input name="beginReceDate" id="beginReceDate" type="text" placeholder="收单日期  自" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'endReceDate\')}'})"/>
										            </div> 
										        </div>
										        <div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_end">
													    <input name="endReceDate" id="endReceDate" type="text" placeholder="至" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'beginReceDate\')}'})"/>
										            </div> 
										        </div>
										        <div class="form-group">
											        <label class="sr-only"></label> 
													<div class="chaxun_input_div">
														<input
															type="text" class="form-control" name="hospitalName"
															id="hospitalName" placeholder="就诊医院名称.....">
													</div> 
											    </div>
										         <div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_begin">
													    <input name="beginCloseCaseDate" id="beginCloseCaseDate" type="text" placeholder="结案日期  自" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'endCloseCaseDate\')}'})"/>
										            </div> 
										        </div>
										        <div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_end">
													    <input name="endCloseCaseDate" id="endCloseCaseDate" type="text" placeholder="至" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'beginCloseCaseDate\')}'})"/>
										            </div> 
										        </div>
										        <div class="form-group" >
													<label class="sr-only"></label> 
													<div class="chaxun_input_div">
														<button type="submit" class="btn btn-primary">查询</button>
													</div> 
										        </div>
											</div>
										</div> -->
									</form>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="box border green" style="padding: 0px 0px 1% 0px;">
									<div class="box-title">
										<h4>
											<i class="fa fa-table"></i>保全查询
										</h4>
									</div>
									<div class="box-body">
										<table id="datatable" style="font-size: 13px;" cellpadding="0"
											cellspacing="0" border="0"
											class="datatable table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th class="center">序号</th>
													<th class="center">保全编号</th>
													<th class="center">被保险人名称</th>
													<th class="center">被保险人证件号</th>
													<th class="center">理赔处理状态</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${pageResult.pageList }" var="item">
													<tr>
														<td class="center">${item.xh }</td>
														<td class="center">${item.chan_id }</td>
														<td class="center">${item.insuName }</td>
														<td class="center">${item.idCard }</td>
														<td class="center">${item.status }</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<div id="paging1" class="page" style="margin-top: 1%"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Edit Modal -->
	<div class="modal fade" id="showSettlementModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel"></div>
	<div class="modal fade" id="showBillInfoModal" tabindex="-1" role="dialog"
  		aria-labelledby="myModalLabel2"></div>	
	<div class="modal fade" id="showIllegalInfoModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel3"></div>	
</body>
<script type="text/javascript" charset="utf-8">
	$(function() {
		$("#paging1")
				.pagination(
						{
							items : parseInt('${pageResult.pages}'),
							currentPage : parseInt('${pageResult.pageNum}'),
							cssStyle : "light-theme",
							prevText : "上一页",
							nextText : "下一页",
							hrefTextPrefix : "${ROOT}/baoquan/baoquanList.html?pageSize=${pageResult.pageSize}&total=${pageResult.total}&pageNum="
						});
		
		$("a.collapse").click(function(){
			var $parentDiv = $(this).parent().parent();
			if($(this).children("i").hasClass("fa-chevron-down")){
				$(this).children("i").removeClass("fa-chevron-down").addClass("fa-chevron-up");
				$parentDiv.next().show();
				$parentDiv.parent().prev().hide();
			}else{
				$(this).children("i").removeClass("fa-chevron-up").addClass("fa-chevron-down");
				$parentDiv.next().hide();
				$parentDiv.parent().prev().show();
			}
		});
		
	});
	
	

	
</script>
</html>
