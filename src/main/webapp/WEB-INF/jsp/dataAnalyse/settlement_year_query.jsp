<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@include file="/WEB-INF/comm/_env.jsp"%>
<title>东风 | 菜单管理</title>
	<style type="text/css">
.control-label.info {
	padding-top: 14px;
}

#areaid.form-control #pbk.form-control {
	width: 100px;
	align: center;
}
</style>
</head>
<body id="main-content" style="overflow:-Scroll;overflow-x:hidden" onload="javascript:load();">
			<div class="container">
			<div class="row">
					<div id="content" class="col-lg-12">
					
						<!-- PAGE HEADER-->
						<div class="row">
						 
								<div class="page-header">
									<ul class="breadcrumb">
									</ul>
									<form class="form-inline" role="form" action="${ROOT}/settlementYear/settlement_Year_Query.html">
										<div class="box">
										<input type="hidden" id="pleve" name="pleve" value="${plevl}"/>
										<input type="hidden" id="pcon" name="pcon" value="${qcon}"/>
										
											<div class="box-body font-400" style="display:block;">
											 <div class="box">
											  <div class="form-group" style="margin-left:50px;">
											  <label class="control-label info">年度:</label> 
											  </div>
												<div class="form-group">
													<div class="date_input_div_begin">
													<input type="hidden" id="checkdate" value="${pstardate}"/>
													    <input name="beginClinDate" id="beginClinDate" type="text" placeholder="选择年份" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({dateFmt:'yyyy',minDate:'2000y',maxDate:'%y'})"value="${pstardate}"/>
										            </div> 
										        </div>
										      
										 	<!--  	<div class="form-group">
												  <div class="list1" id="list1"
													style="padding: 0px; margin: 0px;">

													<label class="control-label info">属地:</label> <input
														type="hidden" class="form-control" name="areaid"
														style="width: 150px;" text="${area }"/> <input type="text"
														class="form-control" id="areaid" readonly="true"
														style="width: 150px;" />
														<div class="select1" id="select1"
															style="border: 1px solid #CCCCCC; display: none; background: #EEEEEE; position: fixed; z-index: 7; min-width: 150px; margin-left: 55px;">
															<ul style="padding-left: 15px; margig-left: 10px;">
																<c:forEach items="${sd}" var="item">
																	<li style="list-style-type:none;"><input
																		type="checkbox" value="${item.item_value}"
																		name="check1" class="chk" />${item.item_nm}</li>
																</c:forEach>
															</ul>
														</div>
												</div>
											</div>  -->
										<div class="form-group" style="margin-left:10px;" id="bkdiv" >
												<div class="list" id="list"
													style="padding: 0px; margin: 0px;">

													<label class="control-label info">板块:</label> 
													<input type="hidden" id="bblock" value="${pbkhidden}"/>
													<input type="hidden" class="form-control" id="pbk" name="pbk" style="width: 150px;"/>
													<input type="text" class="form-control" id="pbk1" name="pbk1" readonly="true" style="width: 150px;"/>
													<div class="select" id="select"
														style="border: 1px solid #CCCCCC; display: none; background: #EEEEEE; position: fixed; z-index: 7; min-width: 150px; margin-left: 38px;">
														<ul style="padding-left: 15px; margig-left: 10px;">
															<c:forEach items="${bk11}" var="item">
																<li style="list-style-type: none;"><input
																	type="checkbox" value="${item.insu_org_id}"
																	name="check" class="chk" />${item.insu_org_nm_ab}</li>
															</c:forEach>
														</ul>
													</div>
												</div>
											</div>
												<div class="form-group"style="margin-left:10px;">
													<label class="control-label info">理赔类型:</label> 
													<input type="hidden" id="lplxs" name="lplxs" value="${lplxhidden}"></input>
													<select name="lplx" class="form-control" style="width:150px;text-align:center;">
													<option value="-1" >全部</option>
													<option ${lplxhidden == '1' ? "selected='selected'" : ""} value="1">线下理赔</option>
													<option ${lplxhidden == '2' ? "selected='selected'" : ""} value="2">医院直赔</option>
													</select>
												</div>
										        <div class="form-group" >
													<label class="sr-only"></label> 
													<div class="chaxun_input_div">
														<button type="submit" class="btn btn-primary-cx">查询</button>
														<button type="button" class="btn btn-primary-xz" onClick="daochuInfo()" id="daochu" >导出 </button>
													 	<!-- <a type="button"  class="btn btn-primary-xz" id="daochu" onclick="daochuInfo();" href="">导出</a> -->
													</div> 
										        	</div>
										        	</div>
										        	</div>
									</form>
								</div>
							
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="box border green" style="padding: 0px 0px 0px 0px;">
									<div class="box-title">
										<h4><i class="fa fa-table"></i>
										<c:if test="${not empty pstardate}">
										<span style="display: inline-block; width:50px;padding-left:7px;">${pstardate}</span>
										</c:if>
										<span style=" display: inline-block;" id="bkbkinfo">
									<c:forEach items="${bk11}" var="item">
										<c:forEach items="${pbkhidden}" var="items">
											<c:if test="${item.insu_org_id == items }">
												<span style="margin-left: 5px; display: inline; ">${item.insu_org_nm_ab }</span>
											</c:if>
										</c:forEach>
									</c:forEach>
									</span>   年度索赔统计报表
										</h4>
									</div>
									<div class="box-body"style="font-size: 13px;overflow-x: auto; overflow-y: auto;">
										<table id="datatable" style="font-size: 13px;width:3000px" cellpadding="0"
											cellspacing="0" border="0"
											class="datatable table table-striped table-bordered table-hover">
											<thead>
												<tr >
													<th class="center" rowspan="3">月份</th>
													<th class="center" colspan="6">独生子女医疗保险</th>
													<th class="center" colspan="6">供养直系亲属医疗保险</th>
													<th class="center" colspan="9">员工意外伤害保险</th>	
													<th class="center" colspan="3" rowspan="2">总计</th>
													<th class="center" rowspan="3">备注</th>
												</tr>
												<tr>
													<th class="center" colspan="3">门诊</th>
													<th class="center" colspan="3">住院</th>
													<th class="center" colspan="3">门诊</th>
													<th class="center" colspan="3">住院</th>
													<th class="center" colspan="3">附加医疗（门诊）</th>
													<th class="center" colspan="3">附加医疗（住院）</th>
													<th class="center" colspan="3">意外伤害</th>
												</tr>
												<tr>
													<th class="center">人次</th>
													<th class="center">申请金额</th>
													<th class="center">赔付金额</th>
													<th class="center">人次</th>
													<th class="center">申请金额</th>
													<th class="center">赔付金额</th>
													<th class="center">人次</th>
													<th class="center">申请金额</th>
													<th class="center">赔付金额</th>
													<th class="center">人次</th>
													<th class="center">申请金额</th>
													<th class="center">赔付金额</th>
													<th class="center">人次</th>
													<th class="center">申请金额</th>
													<th class="center">赔付金额</th>
													<th class="center">人次</th>
													<th class="center">申请金额</th>
													<th class="center">赔付金额</th>
													<th class="center">人次</th>
													<th class="center">申请金额</th>
													<th class="center">赔付金额</th>
													<th class="center" >人次</th>
													<th class="center" >申请金额</th>
													<th class="center" >赔付金额</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${pageResult.pageList }" var="item">
													<tr>
														<td class="center">${item.smonth }</td>
														<td class="center"id="dsznmzrc">${item.dsznmzrc }</td>
														<td class="center">${item.dsznmzsqje }</td>
														<td class="center">${item.dsznmzpfje }</td>
														<td class="center">${item.dsznzyrc }</td>
														<td class="center">${item.dsznzysqje }</td>
														<td class="center">${item.dsznzypfje }</td>
														<td class="center">${item.gygxmzrc }</td>
														<td class="center">${item.gygxmzsqje }</td>
														<td class="center">${item.gygxmzpfje }</td> 
														<td class="center">${item.gygxzyrc }</td>
														<td class="center">${item.gygxzysqje }</td>
														<td class="center">${item.gygxzypfje }</td>
														<td class="center">${item.ygywmzrc }</td>
														<td class="center">${item.ygywmzsqje }</td>
														<td class="center">${item.ygywmzpfje }</td>
														<td class="center">${item.ygywzyrc }</td>
														<td class="center">${item.ygywzysqje }</td>
														<td class="center">${item.ygywzypfje }</td>
														<td class="center">${item.ygywshrc }</td>
														<td class="center">${item.ygywshsqje }</td>
														<td class="center">${item.ygywshpfje }</td>
														<td class="center">${item.sajnum }</td>
														<td class="center">${item.ssqfy }</td>
														<td class="center">${item.spffy }</td>
														<td class="center">${item.lplx }</td>
													</tr>
												</c:forEach>
												<tr id="totalRow">
												<th class="center">合计</th>
												<th class="center">${zjsettlement.hjdsznmzrc }</th>
												<th class="center">${zjsettlement.hjdsznmzsqje }</th>
												<th class="center">${zjsettlement.hjdsznmzpfje }</th>
												<th class="center">${zjsettlement.hjdsznzyrc }</th>
												<th class="center">${zjsettlement.hjdsznzysqje}</th>
												<th class="center">${zjsettlement.hjdsznzypfje }</th>
												<th class="center">${zjsettlement.hjgygxmzrc }</th>
												<th class="center">${zjsettlement.hjgygxmzsqje }</th>
												<th class="center">${zjsettlement.hjgygxmzpfje }</th>
												<th class="center">${zjsettlement.hjgygxzyrc }</th>
												<th class="center">${zjsettlement.hjgygxzysqje }</th>
												<th class="center">${zjsettlement.hjgygxzypfje }</th>
												<th class="center">${zjsettlement.hjygywmzrc }</th>
												<th class="center">${zjsettlement.hjygywmzsqje }</th>
												<th class="center">${zjsettlement.hjygywmzpfje }</th>
												<th class="center">${zjsettlement.hjygywzyrc }</th>
												<th class="center">${zjsettlement.hjygywzysqje }</th>
												<th class="center">${zjsettlement.hjygywzypfje }</th>
												<th class="center">${zjsettlement.hjygywshrc }</th>
												<th class="center">${zjsettlement.hjygywshsqje }</th>
												<th class="center">${zjsettlement.hjygywshpfje }</th>
												<th class="center">${zjsettlement.rczj }</th>
												<th class="center">${zjsettlement.sqjezj }</th>
												<th class="center">${zjsettlement.pfjezj }</th>
												<th class="center"></th>
												</tr>
											</tbody>
											
										</table>
										<div id="paging1" class="page" style="display:none;"></div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
			</div>
		<!-- Edit Modal -->
</body>
<script type="text/javascript" charset="utf-8">
	$(function() {
		var paramStr1="";
		if($('#checkdate').val()!=""){
			paramStr1 = "beginClinDate1="+$("#checkdate").val()+"&";
		}
		var paramStr3="";
		if($("#bblock").val()!=""){
			paramStr3 = "bks="+$("#bblock").val()+"&";
		}
		var paramStr4="";
		if($("#lplxs").val()!=""){
			paramStr4 = "lplxs="+$("#lplxs").val()+"&";
		}
		$("#paging1")
				.pagination(
						{
							items : parseInt('${pageResult.pages}'),
							currentPage : parseInt('${pageResult.pageNum}'),
							cssStyle : "light-theme",
							prevText : "上一页",
							nextText : "下一页",
							hrefTextPrefix : "${ROOT}/settlementYear/settlement_Year_Query.html?"+paramStr1+paramStr3+paramStr4+"pageSize=${pageResult.pageSize}&total=${pageResult.total}&pageNum="
						});
	});
	$(function(){
		//设置板块的选择
		var bk = $("#pbk1");
		bk.click(function() {
			$(".select").slideToggle("slow");
		});
		var sd = $("#areaid");
		sd.click(function() {
			$(".select1").slideToggle("slow");
		});
		//下拉框的高度大于180px时，显示滚动条，反之隐藏
		var iTarget = 150;
		var sel_h = $("#select").height();
		var sel_h1 = $("#select1").height();
		if (sel_h > iTarget) {
			$("#select").css({
				"height" : "150px",
				"overflow-y" : "scroll"
			});
		}
		if (sel_h1 > iTarget) {
			$("#select1").css({
				"height" : "150px",
				"overflow-y" : "scroll"
			});
		}
	});

	//点击其他地方隐藏
	$(document).bind("click", function(event) {
		var e = event || window.event;
		var elem = e.srcElement || e.target;
		while (elem) {
			if (elem.id == "list" || elem.id == "list1") {
				return;
			}
			elem = elem.parentNode;
		}
		ensure();
		$('#select').hide();
	});
	//判断是否选中checkbox的值
	function ensure() {
		var result = [];
		var resultValue = [];
		var check_array = document.getElementsByName("check");
		for (i in check_array) {
			if (check_array[i].checked){
				result.push(check_array[i].nextSibling.nodeValue);
				resultValue.push(check_array[i].value);
			}
		}
		if(result!=""){
			$("#pbk1").val(result);
		}
		
		$("#pbk").val(resultValue);
	};
//	$(function () {
//        var h1 = new Array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0); 
//        $('#datatable tr').each(function () {
//        	for(var i=0;i<h1.length;i++){
 //               $(this).find('td:eq('+(i+1)+')').each(function(){
 //               	h1[i] += parseFloat($(this).text());
 //               });
//        	}
//        });
//        for(var i=0;i<h1.length;i++){
//			$('#totalRow').append("<th class='center'>"+h1[i].toFixed(2)+"</th>")
//		}
 //       $('#totalRow').append("<th class='center'></th>")
//    });
	
	
	
		function daochuInfo(){
			var rqs="";
			if($('#checkdate').val()!=""){
				rqs =$("#checkdate").val();
			}
			var bks="";
			if($("#bblock").val()!=""){
				bks =$("#bblock").val();
			}
			
			var lplx="";
			if($("#lplxs").val()!=""){
				lplx =$("#lplxs").val();
			}
			 /* 采用post提交方式 */
			var link="${ROOT}/exportExcel/exportSettlementYear?";
			/* 首先创建一个form表单   */
			var tempForm = document.createElement("form");    
			tempForm.id="tempForm1";
			/* 制定发送请求的方式为post   */
			tempForm.method="post";   
			/* 此为window.open的url，通过表单的action来实现   */
			tempForm.action=link;  
			/* 利用表单的target属性来绑定window.open的一些参数（如设置窗体属性的参数等） */  
			tempForm.target="_parent";
			/* 创建input标签，用来设置参数   */
		    var hideInput1 = document.createElement("input");    
		    hideInput1.type="hidden";    
		    hideInput1.name= "beginClinDate1";  
		    hideInput1.value= rqs;
		    /* 创建input标签，用来设置参数   */
		    var hideInput2 = document.createElement("input");    
		    hideInput2.type="hidden";    
		    hideInput2.name= "bk";  
		    hideInput2.value= bks;
		    /* 创建input标签，用来设置参数   */
		    var hideInput3 = document.createElement("input");    
		    hideInput3.type="hidden";    
		    hideInput3.name= "lplx";  
		    hideInput3.value= lplx;
		    /*  将input表单放到form表单里   */
		    tempForm.appendChild(hideInput1); 
			tempForm.appendChild(hideInput2); 
			tempForm.appendChild(hideInput3); 
		    /* 将此form表单添加到页面主体body中   */
		    document.body.appendChild(tempForm); 
			/* 手动触发，提交表单   */
		    tempForm.submit();
			/* 从body中移除form表单 */  
		    document.body.removeChild(tempForm);  
		/* var url="${ROOT}/exportExcel/exportSettlementYear?beginClinDate1="+rqs+"&bks="+bks+"&lplx="+lplx; 
		$("#daochu").attr("href",url);  */
		
	}
	
	
	
	$('#exportData').click(function(){
		var rqs="";
		if($('#checkdate').val()!=""){
			rqs =$("#checkdate").val();
		}
		var bks="";
		if($("#bblock").val()!=""){
			bks =$("#bblock").val();
		}
					  $.ajax({
						   type:"POST",
						   url:"${ROOT}/exportExcel/exportSettlementYear",
						   data:{beginClinDate1:rqs,bks:bks},
						   success: function(msg){
							   alert("导出成功！");
						   }
				
				});
	});
	$(function(){
	//	if($("#areaidinfo").find("span").text()!=""){
	//	var str="";
	//	str = $("#areaidinfo").find("span").text();

	//	$("#areaid1").val(str);
	//	}
		if($("#bkbkinfo").text()!=""){
			var strr="";
			strr = $("#bkbkinfo").find("span").text();
			$("#pbk1").val(strr);
		}
	});
	
	$(function(){
		if($("#pleve").val()!=null && $("#pleve").val()!=""){
			if($("#pleve").val()=="2"||$("#pleve").val()=="3"||$("#pleve").val()=="4"){
				document.getElementById("bkdiv").style.display="none";//隐藏
			}
		}
		
	});
	function load(){
		var bkval = new Array;
		var bk = '<%=request.getAttribute("pbkhidden") %>';
		bkval = bk.split(",");
		for(var j=0;j<bkval.length;j++){
		    $("input[name='check']").each(function(i){
				if($(this).val()==bkval[j]){
					$(this).prop("checked", true); 
				}
		    });}
	};
</script>
</html>
