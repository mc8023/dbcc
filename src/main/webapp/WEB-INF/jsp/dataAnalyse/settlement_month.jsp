<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@include file="/WEB-INF/comm/_env.jsp"%>

<title>东风 | 菜单管理</title>
</head>
<body onload="javascript:load();">
	<div id="main-content">
		<div class="container">
			<div class="row">
				<div id="content" class="col-lg-12">
					<!-- PAGE HEADER-->
					<div class="row">
						<div class="page-header">
							<ul class="breadcrumb">
							</ul>
							<form class="form-inline" role="form" action="${ROOT}/lipeiyuebaobiao/lipeiyuebaobiaoList.html">
								<div class="box">
									<div class="box-body font-400" style="display:block;">
										<div class="box">
										<input type="hidden" id="pleve" name="pleve" value="${plevels}"/>
										<input type="hidden" id="pcon" name="pcon" value="${pconds}"/>
									  		<div class="form-group" style="margin-left:50px;">
									  			<label class="control-label info">日期:</label> 
									  		</div>
								        	<div class="form-group">
												<div class="date_input_div_begin">
												<input type="hidden" id="checkdate" name="checkdate" value="${beginClinDate}"/>
													<input name="beginClinDate" id="beginClinDate" type="text"
														placeholder="开始日期 " class="Wdate form-control"
														style="height: 34px;"
														onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'1990y-01M',maxDate:'%y-%M'})"
														value="${beginClinDate}"/>
												</div>
											</div>
								        	
								        	
									       
		
										    <div class="form-group" style="margin-left:10px;padding-top:10px;" id="bkdiv" >
												<div class="list" id="list" style="padding:0px; margin:0px;">
									                <label class="width70 tright">板块:</label>
									                <input type="hidden" id="bblock" value="${pbkhidden}"/>
												    <input type="text" class="form-control" id="bk" readonly="true" style="width:150px;" value="${pbkhidden11}">
												    <input type="hidden" name="bk" value="${pbkhidden}">
												    <div class="select" id="select" style="border: 1px solid #CCCCCC; display: none; background: #EEEEEE; position: fixed; z-index: 999; min-width: 150px;margin-left:38px">
												        <ul style="padding:5px;">
												        	<c:forEach items="${listBk }" var="item">
													            <li style="list-style-type:none;"><input type="checkbox" value="${item.common1 }" name="check" class="chk"/>${item.common2 }</li>
												        	</c:forEach>
												        </ul>
												   </div>
												</div>
										    </div>
										    	<div class="form-group" style="margin-left:5px;">
													<label class="control-label info">理赔类型:</label> 
													<input type="hidden" id="lplxs" name="lplxs" value="${lplxhidden}"></input>
													<select name="lplx" id="lplx" class="form-control" style="width:150px;text-align:center;">
													<!-- <option value="-1" >全部</option>
													<option value="1">线下理赔</option>
													<option value="2">医院直赔</option> -->
												    <option  value="-1">全部</option>
												    <option ${lplxhidden == '1' ? "selected='selected'" : ""} value="1">线下理赔</option>
													<option ${lplxhidden == '2' ? "selected='selected'" : ""} value="2">医院直赔</option>
													</select>
												</div>
									        <div class="form-group" style="margin-left:10px;">
												<div class="chaxun_input_div">
													<label style="width:100px;"></label> 
													<button type="submit" class="btn btn-primary-cx">查询</button>
													<button type="button" class="btn btn-primary-xz"  id="exportData" >导出 </button>
													<!-- <a type="button" class="btn btn-primary-xz" id="exportData">导出</a> -->
													
												</div> 
									        </div>
									    </div>
									</div>
								</div>
							</form>
						</div>
					</div>
						
					<div class="row">
						<div class="col-md-12">
							<div class="box border green" style="padding: 0px 0px 1% 0px;">
								<div class="box-title">
									<h4>
										<i class="fa fa-table"></i>理赔统计月报（含上年同期）
									</h4>
								</div>
								<div class="box-body">
									<table id="datatable" style="font-size: 12px;" cellpadding="0"
										cellspacing="0" border="0"
										class="datatable table table-striped table-bordered table-hover">
										<thead>
											<tr >
												<th class="center" rowspan="2">单位</th>
												<th class="center" rowspan="2">人员类别</th>
												<th class="center" rowspan="2">年度保费</th>
												<th class="center" rowspan="2">累计赔付笔数</th>
												<th class="center" rowspan="2" rowspan="2">累计赔付金额</th>
												<th class="center" rowspan="2">占总赔付率%</th>
												<th class="center" colspan="3">其中：${beginClinDate}月赔付</th>
												<th class="center" rowspan="2">理赔类型</th>
											</tr>
											<tr>
												<th class="center">笔数</th>
												<th class="center">申请金额</th>
												<th class="center">赔付金额</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${listTotal }" var="item">
												<tr> 
													<th class="center" rowspan="6">合计</th> 
													<th class="center">合计</th> 
 													<th class="center">${item.common2}</th> 
 													<th class="center">${item.common3}</th> 
 													<th class="center">${item.common4}</th> 
 													<th class="center">${item.common5}%</th> 
 													<th class="center">${item.common22}</th> 
 													<th class="center">${item.common23}</th> 
 													<th class="center">${item.common24}</th> 
 													<th class="center" rowspan="6"></th> 
 												</tr> 
												<tr>
													<th class="center">独生子女</th>
													<th class="center">${item.common6}</th>
													<th class="center">${item.common10}</th>
													<th class="center">${item.common14}</th>
													<th class="center">${item.common18}%</th>
													<th class="center">${item.common25}</th>
													<th class="center">${item.common26}</th>
													<th class="center">${item.common27}</th>
												</tr>
												
												<tr>
													<th class="center">供养亲属</th>
													<th class="center">${item.common7}</th>
													<th class="center">${item.common11}</th>
													<th class="center">${item.common15}</th>
													<th class="center">${item.common19}%</th>
													<th class="center">${item.common28}</th>
													<th class="center">${item.common29}</th>
													<th class="center">${item.common30}</th>
												</tr>
												<tr>
													<th class="center">员工意外</th>
													<th class="center">${item.common8}</th>
													<th class="center">${item.common12}</th>
													<th class="center">${item.common16}</th>
													<th class="center">${item.common20}%</th>
													<th class="center">${item.common31}</th>
													<th class="center">${item.common32}</th>
													<th class="center">${item.common33}</th>
												</tr>
												<tr>
													<th class="center">劳务工意外</th>
													<th class="center">${item.common9}</th>
													<th class="center">${item.common13}</th>
													<th class="center">${item.common17}</th>
													<th class="center">${item.common21}%</th>
													<th class="center">${item.common34}</th>
													<th class="center">${item.common35}</th>
													<th class="center">${item.common36}</th>
												</tr>
												<tr>
													<th class="center">上年同期</th>
													<th class="center">${item.common37}</th>
													<th class="center">${item.common38}</th>
													<th class="center">${item.common39}</th>
													<th class="center">${item.common43}%</th>
													<th class="center">${item.common40}</th>
													<th class="center">${item.common41}</th>
													<th class="center">${item.common42}</th>
												</tr>
											</c:forEach>
											<c:forEach items="${listInfo }" var="item">
												<tr>
													<th class="center" rowspan="6">${item.common1}</th>
													<th class="center">小计</th>
													<th class="center">${item.common2}</th>
													<th class="center">${item.common3}</th>
													<th class="center">${item.common4}</th>
													<th class="center">${item.common5}%</th>
													<th class="center">${item.common22}</th>
													<th class="center">${item.common23}</th>
													<th class="center">${item.common24}</th>
													<th class="center" rowspan="6">${item.lplxnew}</th>
												</tr>
												<tr>
													<th class="center">独生子女</th>
													<th class="center">${item.common6}</th>
													<th class="center">${item.common10}</th>
													<th class="center">${item.common14}</th>
													<th class="center">${item.common18}%</th>
													<th class="center">${item.common25}</th>
													<th class="center">${item.common26}</th>
													<th class="center">${item.common27}</th>
												</tr>
												<tr>
													<th class="center">供养亲属</th>
													<th class="center">${item.common7}</th>
													<th class="center">${item.common11}</th>
													<th class="center">${item.common15}</th>
													<th class="center">${item.common19}%</th>
													<th class="center">${item.common28}</th>
													<th class="center">${item.common29}</th>
													<th class="center">${item.common30}</th>
												</tr>
												<tr>
													<th class="center">员工意外</th>
													<th class="center">${item.common8}</th>
													<th class="center">${item.common12}</th>
													<th class="center">${item.common16}</th>
													<th class="center">${item.common20}%</th>
													<th class="center">${item.common31}</th>
													<th class="center">${item.common32}</th>
													<th class="center">${item.common33}</th>
												</tr>
												<tr>
													<th class="center">劳务工意外</th>
													<th class="center">${item.common9}</th>
													<th class="center">${item.common13}</th>
													<th class="center">${item.common17}</th>
													<th class="center">${item.common21}%</th>
													<th class="center">${item.common34}</th>
													<th class="center">${item.common35}</th>
													<th class="center">${item.common36}</th>
												</tr>
												<tr>
													<th class="center">上年同期</th>
													<th class="center">${item.common37}</th>
													<th class="center">${item.common38}</th>
													<th class="center">${item.common39}</th>
													<th class="center">${item.common43}%</th>
													<th class="center">${item.common40}</th>
													<th class="center">${item.common41}</th>
													<th class="center">${item.common42}</th>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>  
							</div>
						</div>
					</div>
				</div>
			</div>
	    </div>
	</div>
</body>
<script type="text/javascript" charset="utf-8">
$(function(){
	//日期初始化
	var now = new Date();
//	var beginClinDate = now.getFullYear()+"-"+((now.getMonth()+1)<10?"0":"")+(now.getMonth()+1)+"-"+((now.getDate())-1<10?"0":"")+(now.getDate()-1)
	//var endClinDate = now.getFullYear()+"-"+((now.getMonth()+1)<10?"0":"")+(now.getMonth()+1)+"-"+(now.getDate()<10?"0":"")+now.getDate()
	
//	$("#beginClinDate").val(beginClinDate)
	//$("#endClinDate").val(endClinDate);
	
	//设置板块的选择
	var bk = $("#bk");
    bk.click(function(){
        $(".select").slideToggle("slow");
    });
    
    //下拉框的高度大于180px时，显示滚动条，反之隐藏
    var iTarget = 150;
    var sel_h = $("#select").height();
    if(sel_h > iTarget) {
        $("#select").css({
            "height" : "150px",
            "overflow-y" : "scroll"
        }); 
    }
});
//点击其他地方隐藏
$(document).bind("click",function(event){ 
    var e = event || window.event;   
    var elem = e.srcElement||e.target;   
    while(elem)   
    {   
        if(elem.id == "list")   
        {   
            return;   
        }   
        elem = elem.parentNode;        
    }   
   ensure();
   $('#select').hide();  
});
  
//判断是否选中checkbox的值
function ensure() {
	var resultText = [];
	var resultValue = [];
	
	var check_array = document.getElementsByName("check");
	for(i in check_array) {
	    if(check_array[i].checked) {
	    	resultText.push(check_array[i].nextSibling.nodeValue);
	    	resultValue.push(check_array[i].value);
	    }
	}
    $("#bk").val(resultText);
    $("[name='bk']").val(resultValue);
    $("#select").css("display","none");
}

//导出
$("#exportData").click(function(){
	var beginClinDate = $("#beginClinDate").val();
	//var endClinDate = $("#endClinDate").val();
	var bk = $("[name='bk']").val();
	var lplxs = $("[name='lplx']").val();
	
	/* $.ajax({
		type : "POST",
		url : "${ROOT}/lipeiyuebaobiao/download",
		data:{"beginClinDate":beginClinDate,"endClinDate":endClinDate,"bk":bk},
		dataType : "json",
		traditional: true,
		success : function() {
			alert("导出成功！");
		},
		error : function(request) {
			alert('连接失败,请稍后再试。');
		}
	}); */
	
	 /* 采用post提交方式 */
	var link="${ROOT}/lipeiyuebaobiao/download?";
	/* 首先创建一个form表单   */
	var tempForm = document.createElement("form");    
	tempForm.id="tempForm1";
	/* 制定发送请求的方式为post   */
	tempForm.method="post";   
	/* 此为window.open的url，通过表单的action来实现   */
	tempForm.action=link;  
	/* 利用表单的target属性来绑定window.open的一些参数（如设置窗体属性的参数等） */  
	tempForm.target="_parent";
	/* 创建input标签，用来设置参数   */
    var hideInput1 = document.createElement("input");    
    hideInput1.type="hidden";    
    hideInput1.name= "beginClinDate";  
    hideInput1.value= beginClinDate;
    /* 创建input标签，用来设置参数   */
    var hideInput2= document.createElement("input");    
    hideInput2.type="hidden";    
    hideInput2.name= "bk";  
    hideInput2.value= bk;
    /* 创建input标签，用来设置参数   */
    var hideInput3 = document.createElement("input");    
    hideInput3.type="hidden";    
    hideInput3.name= "lplx";  
    hideInput3.value= lplxs;
    /*  将input表单放到form表单里   */
    tempForm.appendChild(hideInput1); 
	tempForm.appendChild(hideInput2); 
	tempForm.appendChild(hideInput3); 
    /* 将此form表单添加到页面主体body中   */
    document.body.appendChild(tempForm); 
	/* 手动触发，提交表单   */
    tempForm.submit();
	/* 从body中移除form表单 */  
    document.body.removeChild(tempForm);  
	/* var url="${ROOT}/lipeiyuebaobiao/download?beginClinDate="+beginClinDate+"&bk="+bk+"&lplx="+lplxs; 
	$("#exportData").attr("href",url);  */
});

$(function(){
	if($("#pleve").val()!=null && $("#pleve").val()!=""){
		if($("#pleve").val()=="2"||$("#pleve").val()=="3"||$("#pleve").val()=="4"){
			document.getElementById("bkdiv").style.display="none";//隐藏
		}
	}
	
});
function load(){
	var bkval = new Array;
	var bk = '<%=request.getAttribute("pbkhidden") %>';
	bkval = bk.split(",");
	for(var j=0;j<bkval.length;j++){
	    $("input[name='check']").each(function(i){
			if($(this).val()==bkval[j]){
				$(this).prop("checked", true); 
			}
	    });}
	var resultText = [];
	var check_array = document.getElementsByName("check");
	for(i in check_array) {
	    if(check_array[i].checked) {
	    	resultText.push(check_array[i].nextSibling.nodeValue);
	    }
	}
    $("#bk").val(resultText);
};

</script>
</html>
