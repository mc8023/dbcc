<%@page pageEncoding="UTF-8"%>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Cloud Admin | Dynamic Tables</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<%@include file="/WEB-INF/comm/_env.jsp"%>
</head>
<body>
	<%@include file="/WEB-INF/comm/_header.jsp"%>
	<section id="page">
		<%@include file="/WEB-INF/comm/_left.jsp"%>
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<ul class="breadcrumb">
										<li><i class="fa fa-home"></i> 首页</li>
										<li>数据查询</li>
										<li>理赔查询</li>
									</ul>
									<form class="form-inline" role="form">
										<div class="form-group">
											<label class="sr-only">Email address</label> <input
												type="text" class="form-control" id="mailText"
												placeholder="">
										</div>
										<div class="form-group">
											<label class="sr-only">Password</label> <input
												type="password" class="form-control"
												id="exampleInputPassword2" placeholder="">
										</div>
										<button type="submit" class="btn btn-primary-cx">查询</button>



										<div class="box">
											<div class="box-title">
												<div class="tools" style="margin-right: 47%">
													<a href="javascript:;" class="collapse"> <span>高级查询</span>
														<i class="fa fa-chevron-up"></i>
													</a>
												</div>
											</div>
											<div class="box-body font-400">
												<div class="form-group">
													<label class="sr-only">Email address</label> <input
														type="text" class="form-control" id="mailText"
														placeholder="">
												</div>
												<div class="form-group">
													<label class="sr-only">Password</label> <input
														type="password" class="form-control"
														id="exampleInputPassword2" placeholder="">
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="box border green" style="padding: 0px 0px 1% 0px;">
									<div class="box-title">
										<h4>
											<i class="fa fa-table"></i>Dynamic Data Tables
										</h4>
									</div>
									<div class="box-body">
										<table id="datatable" style="font-size: 13px;" cellpadding="0"
											cellspacing="0" border="0"
											class="datatable table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>被保险人ID</th>
													<th>被保险人名称</th>
													<th>险种</th>
													<th>总保额</th>
													<th>剩余赔付金额</th>
													<th>已赔付金额</th>
													<th>操作</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${pageResult.pageList }" var="item">
													<tr>
														<td>${item.insuId }</td>
														<td>${item.insuNm }</td>
														<td class="hidden-xs">${item.productId }</td>
														<td class="center">${item.totalInsuAmt }</td>
														<td class="center">${item.remaInsuAmt }</td>
														<td class="center">${item.accuLoss }</td>
														<td class="center hidden-xs"><a href="#">查看详情</a></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<div id="paging1" class="page" style="margin-top: 1%"></div>
									</div>
								</div>
							</div>
						</div>
						<%@include file="/WEB-INF/comm/_footer.jsp"%>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
<script type="text/javascript" charset="utf-8">
	$(function() {
		$("#paging1")
				.pagination(
						{
							items : parseInt('${pageResult.pages}'),
							currentPage : parseInt('${pageResult.pageNum}'),
							cssStyle : "light-theme",
							prevText : "上一页",
							nextText : "下一页",
							hrefTextPrefix : "${ROOT}/result/list.html?pageSize=${pageResult.pageSize}&total=${pageResult.total}&pageNum="
						});
	});
</script>
</html>
