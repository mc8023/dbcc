<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8" />
<title>东风 | 角色管理</title>
<%@include file="/WEB-INF/comm/_env.jsp"%>
</head>
<body>
	<div id="main-content">
		<div class="container">
			<div class="row">
				<div id="content" class="col-lg-12">
					<!-- PAGE HEADER-->
					<div class="row">
						<div class="col-sm-12">
							<div class="page-header">
							    <ul class="breadcrumb"></ul>
								<form action="${ROOT}/sys/role.html" class="form-inline"
									role="form">
									<div class="form-group">
										<label class="sr-only">角色名称</label> <input type="text"
											class="form-control" id="rolename" name="rolename"
											placeholder="请输入角色名称" value="${rolename }">
									</div>
									<button type="submit" class="btn btn-primary-cx">查询</button>
								</form>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="box border green" style="padding: 0px 0px 1% 0px;">
								<div class="box-title">
									<h4>
										<i class="fa fa-table"></i>系统角色
									</h4>
									<div style="float: right;">
										<button class="btn btn-xs btn-info" onclick="editRole(0);"><i class="fa fa-plus"></i>添加</button>
									</div>
								</div>
								<div class="box-body">
									<table id="datatable" style="font-size: 13px;" cellpadding="0"
										cellspacing="0" border="0"
										class="datatable table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>ID</th>
												<th>角色名称</th>
												<th>状态</th>
												<th>描述</th>
												<th>操作</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${pageResult.pageList }" var="item">
												<tr${item.id}>
													<td>${item.id }</td>
													<td>${item.name }</td>
													<td class="center">
														<c:choose>
															<c:when test="${item.status==0}">
																有效
															</c:when>
															<c:when test="${item.status==1}">
																无效
															</c:when>
															<c:otherwise>
																未知
															</c:otherwise>
														</c:choose>
													</td>
													<td>${item.description }</td>
													<td class="center hidden-xs">
														<a class="btn btn-xs btn-info" href="javascript:editRole(${item.id}),void(0)">
															<i class="fa fa-pencil-square-o"></i> 编辑
														</a>
														<a class="btn btn-xs btn-danger" href="javascript:delConfirm(${item.id});">
															<i class="fa fa-trash-o"></i> 删除</a>
														<a class="btn btn-xs btn-success" href="javascript:editRoleFunc(${item.id}),void(0)">
															<i class="fa fa-check-square-o"></i> 分配菜单</a>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
									<div id="paging1" class="page" style="margin-top: 1%"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Edit Modal -->
	<div class="modal fade" id="editRoleModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">角色编辑</h4>
				</div>
				<form action="${ROOT}/sys/role/post" id="editForm"
					class="form-horizontal" role="form">
					<div id="tips" align="center"></div>
					<div class="modal-body">
						<div class="form-group">
							<label class="col-sm-3 control-label">ID：</label>
							<div class="col-sm-9">
								<input class="form-control input-sm" type="text" name="id" id="id" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">角色名称：</label>
							<div class="col-sm-9">
								<input class="form-control input-sm" type="text" name="name" id="name"
									placeholder="请输入角色名称">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">角色状态：</label>
							<div class="col-sm-9">
								<label class="radio-inline"> <input type="radio" id="rad11"
									class="uniform" name="status" value="0"> 有效
								</label> <label class="radio-inline"> <input type="radio" id="rad12"
									class="uniform" name="status" value="1"> 无效
								</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">描述：</label>
							<div class="col-sm-9">
								<textarea class="form-control"  rows="5" cols="50" name="description"></textarea>
							</div>
						</div>
						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button class="btn btn-primary" onclick="submit_close(this)">提交并关闭</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	<!-- delete Modal -->
	<div class="modal fade" id="deleteRoleModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<input type="hidden" id="delId" value="" />
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">角色删除确认</h4>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<!-- <button type="submit" class="btn btn-primary">保存</button> -->
						<button class="btn btn-danger" onclick="del()">确认删除</button>
					</div>
			</div>
		</div>
	</div>
	
	<!-- Edit Role Func -->
	<div class="modal fade" id="editRoleFuncModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
		<input type="hidden" id="roleId" name="roleId" value="">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">选择菜单</h4>
					
					
				</div>
				<div class="modal-body">
						<table class="table table-hover" id='role_func_tab'>
						<thead>
						  <tr>
							<th><input type="checkbox" name="selectFuncAll" id="selectFuncAll" onchange="selectFuncAll">全选</th>
							<th>菜单名称</th>
							<th>菜单等级</th>
							<th>状态</th>
						  </tr>
						</thead>
						<tbody>
						<c:forEach items="${funcList}" var="item" varStatus="status">
						  <tr>
						    
							<td><input type="checkbox" name="funcs" class='uniform' value='${item.id }' id="demo" onclick="selectAll('${item.name}');"></td>
							<td>${item.name }</td>
							<td>
							<c:if test="${item.name=='首页' || item.name=='系统管理' ||  item.name=='数据分析' || item.name=='数据查询'}">
   							一级菜单
							</c:if>
							<c:if test="${item.name!='首页' && item.name!='系统管理' &&  item.name!='数据分析' && item.name!='数据查询'}">
   							二级菜单
							</c:if>
							</td>
							<td>${item.status==0?'有效':'无效' }</td>
						  </tr>
						 </c:forEach>
						</tbody>
					  </table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button class="btn btn-primary" onclick="submit_close_func(this)">提交并关闭</button>
				</div>
			</div>
		</div>
	</div>
	
	
</body>
<script type="text/javascript" charset="utf-8">

$(function () {
	$("input[type=radio]").attr("checked",'0');
	$("#paging1").pagination({
		items : parseInt('${pageResult.pages}'),
		currentPage : parseInt('${pageResult.pageNum}'),
		cssStyle : "light-theme",
		prevText : "上一页",
		nextText : "下一页",
		hrefTextPrefix : "${ROOT}/role/list.html?pageSize=${pageResult.pageSize}&total=${pageResult.total}&pageNum="
	});
	
	
    var ajax = null, $fm = $('#editRoleModal form:first'), $md = $('#editRoleModal');
    window.editRole = function (id) {
        var fm = $fm[0];
        if (ajax)
            ajax.abort();
        fm.reset();
        ajax = $.getJSON("${ROOT}/sys/role/get/" + id, function (json) {
            if (id != 0 && !json.data) {
                return alert("找不到用户");
            }
            if (id == 0 && !json.data) {
                $md.find(".modal-header h4").html("新增角色");
                json = $.extend(true, {}, json.data, {data: {id: 0}});
            } else {
                $md.find(".modal-header h4").html("编辑角色 - " + (json.data.name || ""));
                if(json.data.status==0){
                	rad11.checked = "checked";
                }else{
                	rad12.checked = "checked";
                }
            }
            var $fm = $(fm);
            $fm.unSerializeObjectFromJson(json.data);
            $("#editRoleModal .progress").hide();
            $fm.removeClass("hide");
        });
        $md.modal();
    };

    $md.on('hidden', function (e) {
        $("#editRoleModal .progress").show();
        $fm.addClass("hide");
    });
    $.request($fm, function (json) {
        if (json.success) {
            alert("保存成功!");
            window.location.reload();
        }
        if (ajax.__close) {
            $md.modal("hide");
        }
    });
    window.submit_close = function (dom) {
    	var flag = true;
    	var rolename = $("#rolename").val();
    	if(rolename==""){
    		$("#rolename").focus();
    		flag = false;
    		//return alert("角色名称为必填项!");
    	}
    	if(flag){
	        var the = $(dom);
	        the.closest(".modal").find("form:first").submit();
	        ajax.__close = true;
	        window.close();
        }
    };

    
    delConfirm=function(id){
    	$("#deleteRoleModal .modal-body").html("确定删除id为"+id+"的记录吗?");
    	$('#deleteRoleModal').modal("show");
    	$("#delId").val(id);
    }
    del=function(){
    	var delId  = $("#delId").val();
    	 $.ajax({
			type : "POST",
			url : "${ROOT}/sys/role/delete/"+delId,
			dataType : "json",
			success : function(json) {
				if(json.success){
					debugger;
					alert("删除成功！");
					$("#tr"+delId).remove();
					$("#deleteRoleModal").modal("hide");
				}else {
					alert(data.error);
				}
			}
		}); 
    }
    
    window.editRoleFunc = function (id) {
    	$("#roleId").val(id);
    	$.ajaxSettings.async = false; 
    	$.getJSON("${ROOT}/sys/role_func/getByRoleId/" + id, function (json) {
    		$("#role_func_tab tbody :checkbox").each(function(){
    			$(this).removeAttr("checked");
        	});
    		 debugger;
			 var content = json.data;
			
    		$("#role_func_tab tbody :checkbox").each(function(){
    			var the = $(this);
    			for(var i = 0;i<content.length;i++){
    			
    				if(the.val()==content[i].funcId){
    					$(this).prop("checked","checked");
    				}
    			}
        	}); 
    	    $("#editRoleFuncModal").modal();
    	});	
    }
    
    selectAllFunc=function(){
    	// 全选
    	var allDom = document.getElementsByName("selectFuncAll")[0];  //找到全选框
    	var numDoms = document.getElementsByName("funcs"); //找到选项的数组

    	allDom.onclick = function(){   //定义一个全选框的点击函数
    	if(this.checked){                 //判断全选框是否选中,如果返回值为true代表选中
    			for(var i=0;i<numDoms.length;i++){   //使用for循环选中列表框
    				numDoms[i].checked = true;
    				//$('#daochu').removeAttr("disabled");
    			}
    	}else{                                             //如果返回值为false代表未选中
    			for(var i=0;i<numDoms.length;i++){  //使用for循环取消列表框的选中状态
    				numDoms[i].checked = false;
    				//$('#daochu').attr('disabled',"true");
    			}
    		}
    	}
	}
    
 // 全选
	var allDom = document.getElementsByName("selectFuncAll")[0];  //找到全选框
	var numDoms = document.getElementsByName("funcs"); //找到选项的数组

	allDom.onclick = function(){   //定义一个全选框的点击函数
	if(this.checked){                 //判断全选框是否选中,如果返回值为true代表选中
			for(var i=0;i<numDoms.length;i++){   //使用for循环选中列表框
				numDoms[i].checked = true;
				//$('#daochu').removeAttr("disabled");
			}
	}else{                                             //如果返回值为false代表未选中
			for(var i=0;i<numDoms.length;i++){  //使用for循环取消列表框的选中状态
				numDoms[i].checked = false;
				//$('#daochu').attr('disabled',"true");
			}
		}
	}
    

	
	
    window.submit_close_func=function(dom){
    	var id_array = new Array();
    	$("#role_func_tab tbody :checkbox").each(function(){
    		if($(this).is(":checked")){
    			id_array.push($(this).val())
    		}
    	});
    	if(id_array.length<=0){
    		alert("请选择菜单！");
    		return;
    	}
    	$.ajax({
    		url:"${ROOT}/sys/role_func/post/",
    		type:"POST",
    		dataType:"json",
    		data:"ids="+id_array+"&roleId="+$("#roleId").val(),
    		success:function(json){
    			if(json.success){
    				alert("保存成功！");
        			$("#editRoleFuncModal").modal("hide");
    			}else{
    				alert("保存失败！");
    			}
    		}
    	});
    }
    
    
});

function selectAll(name){
	
	 if($("input[type='checkbox']").is(':checked') && ("用户管理"==name || "角色管理"==name) ){
		 $("#role_func_tab").find("tr").each(function() {
             if($(this).children().eq(1).text() == "系统管理") {
            	 $(this).children().eq(0).find(":checkbox").prop("checked","checked");
	          }
         });
	 }
	 
	 $("#role_func_tab").find("tr").each(function() {
         if($(this).children().eq(1).text() == "系统管理" &&  $(this).children().eq(0).find(":checkbox").is(':checked')==false) {
        	 $("#role_func_tab").find("tr").each(function() {
        		 if($(this).children().eq(1).text() == "用户管理" || $(this).children().eq(1).text() == "角色管理"){
        			 $(this).children().eq(0).find(":checkbox").removeAttr("checked");
        		 }
        	 })
        	
           }
     });
	 
	 if($("input[type='checkbox']").is(':checked') && ("理赔明细查询"==name || "年度索赔统计报表"==name || "理赔统计月报"==name || "年度理赔台账查询"==name) ){
		 $("#role_func_tab").find("tr").each(function() {
             if($(this).children().eq(1).text() == "数据分析") {
            	 $(this).children().eq(0).find(":checkbox").prop("checked","checked");
	           }
         });
	 }

	 
	 $("#role_func_tab").find("tr").each(function() {
         if($(this).children().eq(1).text() == "数据分析" &&  $(this).children().eq(0).find(":checkbox").is(':checked')==false) {
        	 $("#role_func_tab").find("tr").each(function() {
        		 if($(this).children().eq(1).text() == "理赔明细查询" || $(this).children().eq(1).text() == "年度索赔统计报表" || $(this).children().eq(1).text() == "理赔统计月报" ||  $(this).children().eq(1).text() == "年度理赔台账查询"){
        			 $(this).children().eq(0).find(":checkbox").removeAttr("checked");
        		 }
        	 })
        	
           }
     });
	 
	 if($("input[type='checkbox']").is(':checked') && ("综合查询"==name) ){
		 $("#role_func_tab").find("tr").each(function() {
             if($(this).children().eq(1).text() == "数据查询") {
            	 $(this).children().eq(0).find(":checkbox").prop("checked","checked");
	           }
         });
	 }
	 
	 $("#role_func_tab").find("tr").each(function() {
         if($(this).children().eq(1).text() == "数据查询" &&  $(this).children().eq(0).find(":checkbox").is(':checked')==false) {
        	 $("#role_func_tab").find("tr").each(function() {
        		 if($(this).children().eq(1).text() == "综合查询"){
        			 $(this).children().eq(0).find(":checkbox").removeAttr("checked");
        		 }
        	 })
        	
           }
     });
	 
		 
		 
		 
		 
	 
	 
	
}


</script>
</html>
