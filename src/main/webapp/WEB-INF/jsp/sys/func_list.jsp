<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8" />
<%@include file="/WEB-INF/comm/_env.jsp"%>
<title>东风 | 菜单管理</title>
</head>
<body>
		<div id="main-content">
		<div class="container">
			<div class="row">
				<div id="content" class="col-lg-12">
					<!-- PAGE HEADER-->
					<div class="row">
						<div class="col-sm-12">
							<div class="page-header">
								<ul class="breadcrumb">
								</ul>
								<form action="${ROOT}/sys/func.html" class="form-inline"
									role="form">
									<div class="form-group">
										<label class="sr-only">菜单名称</label> <input type="text"
											class="form-control" id="funcname" name="funcname"
											placeholder="请输入菜单名称" value="${funcname }">
									</div>
									<button type="submit" class="btn btn-primary-cx">查询</button>
								</form>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="box border green" style="padding: 0px 0px 1% 0px;">
								<div class="box-title">
									<h4>
										<i class="fa fa-table"></i>系统菜单
									</h4>
									<div style="float: right;">
										<button class="btn btn-xs btn-info" onclick="editFunc(0);"><i class="fa fa-plus"></i>添加</button>
									</div>
								</div>
								<div class="box-body">
									<table id="datatable" style="font-size: 13px;" cellpadding="0"
										cellspacing="0" border="0"
										class="datatable table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>ID</th>
												<th>菜单名称</th>
												<th>菜单等级</th>
												<th>URL</th>
												<th>父菜单</th>
												<th>排序号</th>
												<th>状态</th>
												<th>操作</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${pageResult.pageList }" var="item">
												<tr${item.id}>
													<td>${item.id }</td>
													<td>${item.name }</td>
													<td>${item.func_level }</td>
													<td>${item.url }</td>
													<td>${item.parent }</td>
													<td>${item.seq }</td>
													<td class="center">
														<c:choose>
															<c:when test="${item.status==0}">
																有效
															</c:when>
															<c:when test="${item.status==1}">
																无效
															</c:when>
															<c:otherwise>
																未知
															</c:otherwise>
														</c:choose>
													</td>
													<td class="center hidden-xs">
														<a class="btn btn-xs btn-info" href="javascript:editFunc(${item.id}),void(0)">
															<i class="fa fa-pencil-square-o"></i> 编辑
														</a>
														<a class="btn btn-xs btn-danger" href="javascript:delConfirm(${item.id});">
															<i class="fa fa-trash-o"></i> 删除</a>
															
														<%-- <button class="btn btn-xs btn-info" onclick="javascript:delConfirm(${item.id});"><i class="fa fa-trash-o"></i>添加</button> --%>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
									<div id="paging1" class="page" style="margin-top: 1%"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Edit Modal -->
	<div class="modal fade" id="editFuncModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">用户编辑</h4>
				</div>
				<form action="${ROOT}/sys/func/post" id="editForm"
					class="form-horizontal" role="form">
					<div id="tips" align="center"></div>
					<div class="modal-body">
						<div class="form-group">
							<label class="col-sm-3 control-label">ID：</label>
							<div class="col-sm-9">
								<input class="form-control input-sm" type="text" name="id" id="id" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">菜单名称：</label>
							<div class="col-sm-9">
								<input class="form-control input-sm" type="text" name="name" id="name"
									placeholder="请输入菜单名称">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">菜单等级：</label>
							<div class="col-sm-9">
								<label class="radio-inline"> <input type="radio"
									class="uniform" name="func_level" value="1"> 一级菜单
								</label> <label class="radio-inline"> <input type="radio"
									class="uniform" name="func_level" value="2"> 二级菜单
								</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">菜单URL：</label>
							<div class="col-sm-9">
								<input class="form-control input-sm" type="text" name="url" id="url"
									placeholder="请输入菜单名称">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">父菜单：</label>
							<div class="col-sm-9">
								<select class="form-control" name="parent" id="parent">
									<option value="-1">---请选择---</option>
									<div id="isShowFunc">
										<c:forEach items="${firstFuncList}" var="item">
											<option value="${item.id }">${item.name }</option>
										</c:forEach>
									</div>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">排序号：</label>
							<div class="col-sm-9">
								<input class="form-control input-sm" type="text"
									name="seq" placeholder="请输入排序号">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">用户状态：</label>
							<div class="col-sm-9">
								<label class="radio-inline"> <input type="radio"
									class="uniform" name="status" value="0"> 有效
								</label> <label class="radio-inline"> <input type="radio"
									class="uniform" name="status" value="1"> 无效
								</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">ICON样式：</label>
							<div class="col-sm-9">
								<input class="form-control input-sm" type="text"
									name="icon" placeholder="请输入ICON字符串样式，例如：fa fa-table fa-fw">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">描述：</label>
							<div class="col-sm-9">
								<textarea class="form-control"  rows="5" cols="50" name="description"></textarea>
							</div>
						</div>
						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button class="btn btn-primary" onclick="submit_close(this)">提交并关闭</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	<!-- delete Modal -->
	<div class="modal fade" id="deleteFuncModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<input type="hidden" id="delId" value="" />
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">菜单删除确认</h4>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<!-- <button type="submit" class="btn btn-primary">保存</button> -->
						<button class="btn btn-danger" onclick="del()">确认删除</button>
					</div>
			</div>
		</div>
	</div>
	
	
</body>
<script type="text/javascript" charset="utf-8">

$(function () {
	$("#paging1").pagination({
		items : parseInt('${pageResult.pages}'),
		currentPage : parseInt('${pageResult.pageNum}'),
		cssStyle : "light-theme",
		prevText : "上一页",
		nextText : "下一页",
		hrefTextPrefix : "${ROOT}/func/list.html?pageSize=${pageResult.pageSize}&total=${pageResult.total}&pageNum="
	});
	
	
    var ajax = null, $fm = $('#editFuncModal form:first'), $md = $('#editFuncModal');
    window.editFunc = function (id) {
        var fm = $fm[0];
        if (ajax)
            ajax.abort();
        fm.reset();
        ajax = $.getJSON("${ROOT}/sys/func/get/" + id, function (json) {
            if (id != 0 && !json.data) {
                return alert("找不到用户");
            }
            if (id == 0 && !json.data) {
                $md.find(".modal-header h4").html("新增菜单");
                json = $.extend(true, {}, json.data, {data: {id: 0}});
            } else {
                $md.find(".modal-header h4").html("编辑菜单 - " + (json.data.name || ""));
            }
            var $fm = $(fm);
            $fm.unSerializeObjectFromJson(json.data);
            $("#editFuncModal .progress").hide();
            $fm.removeClass("hide");
        });
        $md.modal();
    };

    
    $md.on('hidden', function (e) {
        $("#editFuncModal .progress").show();
        $fm.addClass("hide");
    });
    $.request($fm, function (json) {
        if (json.success) {
            alert("保存成功!");
            window.location.reload();
        }
        if (ajax.__close) {
            $md.modal("hide");
        }
    });
    window.submit_close = function (dom) {
        var the = $(dom);
        the.closest(".modal").find("form:first").submit();
        ajax.__close = true;
    };

    
    delConfirm=function(id){
    	$("#deleteFuncModal .modal-body").html("确定删除id为"+id+"的记录吗?");
    	$('#deleteFuncModal').modal("show");
    	$("#delId").val(id);
    }
    del=function(){
    	var delId  = $("#delId").val();
    	 $.ajax({
			type : "POST",
			url : "${ROOT}/sys/func/delete/"+delId,
			dataType : "json",
			success : function(json) {
				if(json.success){
					debugger;
					alert("删除成功！");
					$("#tr"+delId).remove();
					$("#deleteFuncModal").modal("hide");
				}else {
					alert(data.error);
				}
			}
		}); 
    }
});
</script>
</html>
