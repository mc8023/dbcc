<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@include file="/WEB-INF/comm/_env.jsp"%>
<title>东风 | 菜单管理</title>
</head>
<script src="http://cdn.bootcss.com/blueimp-md5/1.1.0/js/md5.js"></script>
<body>
			
			<div id="main-content" >
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						
									<h4 class="modal-title" id="myModalLabel">修改密码</h4>
									<form class="form-inline" role="form" action="${ROOT}/sys/editpsw.html">
									<input type="hidden" name="psw" id="psw" value="${password}"/>
										<div >
											<div class="form-group col-sm-12">
											<div class=" col-sm-2" style="margin-top:18px;margin-left:50px">
											<label >原密码  ：</label> </div>
												<div class="chaxun_input_div col-sm-6">
												<input class="form-control " type="password" name="psw1" id="psw1"
												placeholder="请输入">
												</div>
											</div>
											
											
											<div class="form-group col-sm-12">
											<div class=" col-sm-2" style="margin-top:18px;margin-left:50px">
											<label >新密码  ：</label> </div>
												<div class="chaxun_input_div col-sm-6">
												<input class="form-control " type="password" name="psw2" id="psw2"
												placeholder="请输入">
												</div>
											</div>
											
											<div class="form-group col-sm-12">
											<div class=" col-sm-2" style="margin-top:18px;margin-left:50px">
											<label >确认密码：</label> </div>
												<div class="chaxun_input_div col-sm-6">
												<input class="form-control " type="password" name="psw3" id="psw3"
												placeholder="请输入">
												</div>
											</div>
											<div class="form-group col-sm-12">
												<div class="modal-footer">
												<button class="btn btn-primary" type="button" onclick="close_role()">确认</button>
												</div>
											</div>
										</div>
										
										
										
											
									</form>
								
						
					
					</div>
				</div>
			</div>
		</div>
		
		
	

</body>
<script type="text/javascript" charset="utf-8">

	function close_role(){
		
		if($("#psw1").val()!=null&&$("#psw1").val().length>0){
			
		}else{
			alert("请输入密码！");
			return false;
		}

		if($("#psw2").val()!=null&&$("#psw2").val().length>0){
			
		}else{
			alert("请输入新密码！");
			return false;
		}
		
		if($("#psw3").val()!=null&&$("#psw3").val().length>0){
			
		}else{
			alert("请输入确认密码！");
			return false;
		}
		
		
		
		var hash = md5($("#psw1").val());
		var hsah2= md5($("#psw2").val());
		var hsah3= md5($("#psw3").val());
		
		if($("#psw").val()!=hash){
			alert("密码输入不正确，请重新输入！");
			$("#psw1").val("");
			return false;
		}
		
		
		if($("#psw2").val()!=$("#psw3").val()){
			alert("新密码不一致请确认并重新输入！");
			return false;
		}
		
		
		   $.ajax({
	       		url: "${ROOT}/sys/user/pwd/",      		
	       		type:"POST",
	       		dataType:"json",
	       		data:"psw1="+hash+"&psw2="+hsah2+"&psw3="+hsah3,
	       		success:function(json){
	       			if(json.success){
	    				alert("保存成功！");
	    				$("#psw").val(hsah2);
	    				$("#psw1").val("");
	    				$("#psw2").val("");
	    				$("#psw3").val("");
	    				
	    			}else{
	    				alert("保存失败！");
	    			}
	       		}
	       	});
	}
	
	
</script>
</html>
