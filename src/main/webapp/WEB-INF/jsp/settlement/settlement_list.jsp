 <%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@include file="/WEB-INF/comm/_env.jsp"%>
<title>东风 | 菜单管理</title>
</head>
<body>
			<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header" >
									<ul class="breadcrumb">
									</ul>
									<div class="modal-header settlementlist" style="border-bottom:none;">

						 		<span class="icono-cross" id="fpxqff" data-dismiss="modal" style="display:inline-block;float:right;margin-top:-5px;color:#7e7e7e;"/>
						 
										<h4 class="modal-title" id="myModalLabel2">发票详情</h4>
									</div> 
									<form class="form-inline" role="form" action="${ROOT}/settlement/settlementList.html"  hidden="hidden">
										<div class="form-group">
											<label class="sr-only"></label>
											<div class="chaxun_input_div">
											  <input type="text" class="form-control" id="caseId" name="caseId" placeholder="索赔编号....." value="${case_Id}">
											</div> 
											
										</div>
										<div class="form-group">
											<label class="sr-only"></label> 
											<div class="chaxun_input_div">
												<input
													type="text" class="form-control" name="settlementStatus"
													id="settlementStatus" placeholder="理赔处理状态....." value="${settlement_Status}">
											</div> 
										</div>
										<div class="form-group">
											<label class="sr-only"></label> 
											<div class="chaxun_input_div">
												<input
													type="text" class="form-control" name="insuName"
													id="insuName" placeholder="被保险人姓名....." value="${insu_Name}">
											</div> 
										</div>
										
										<div class="form-group">
											<label class="sr-only"></label> 
											<div class="chaxun_input_div">
												<input
													type="text" class="form-control" name="idCard"
													id="idCard" placeholder="被保险人证件号....." value="${id_Card}">
											</div> 
										</div>
										<div class="form-group">
											<label class="sr-only"></label> 
											<div class="chaxun_input_div">
												<input
													type="text" class="form-control" name="miCardId"
													id="miCardId" placeholder="被保险人医保号....." value="${mi_CardId}">
											</div> 
										</div>
										<div class="form-group">
											<label class="sr-only"></label> 
											<div class="chaxun_input_div">
												<button type="submit" class="btn btn-primary-cx">查询</button>
											</div> 
										</div>

										<div class="box">
											<div class="box-title">
												<div class="tools" style="margin-right: 47%">
													<a href="javascript:;" class="collapse"> <span>高级查询</span>
														<i class="fa fa-chevron-down"></i>
													</a>
												</div>
											</div>
											<div class="box-body font-400" style="display: none;">
											   <div class="form-group">
											        <label class="sr-only"></label> 
													<div class="chaxun_input_div">
														<input
															type="text" class="form-control" name="medYear"
															id="medYear" placeholder="可选投保年度....." value="${med_Year}">
													</div> 
											    </div>
												<div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_begin">
													    <input name="beginClinDate" id="beginClinDate" type="text" placeholder="就诊日期  自" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'endClinDate\')}'})" value="${begin_Clin_Date}"/>
										            </div> 
										        </div>
										        <div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_end">
													    <input name="endClinDate" id="endClinDate" type="text" placeholder="至" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'beginClinDate\')}'})" value="${end_Clin_Date}"/>
										            </div> 
										        </div>
										        <div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_begin">
													    <input name="beginReceDate" id="beginReceDate" type="text" placeholder="收单日期  自" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'endReceDate\')}'})" value="${begin_Rece_Date}"/>
										            </div> 
										        </div>
										        <div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_end">
													    <input name="endReceDate" id="endReceDate" type="text" placeholder="至" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'beginReceDate\')}'})" value="${end_Rece_Date}"/>
										            </div> 
										        </div>
										        <div class="form-group">
											        <label class="sr-only"></label> 
													<div class="chaxun_input_div">
														<input
															type="text" class="form-control" name="hospitalName"
															id="hospitalName" placeholder="就诊医院名称....."  value="${hospital_Name}">
													</div> 
											    </div>
										         <div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_begin">
													    <input name="beginCloseCaseDate" id="beginCloseCaseDate" type="text" placeholder="结案日期  自" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'endCloseCaseDate\')}'})" value="${begin_Close_Case_Date}"/>
										            </div> 
										        </div>
										        <div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_end">
													    <input name="endCloseCaseDate" id="endCloseCaseDate" type="text" placeholder="至" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'beginCloseCaseDate\')}'})" value="${end_Close_Case_Date}"/>
										            </div> 
										        </div>
										        <div class="form-group" >
													<label class="sr-only"></label> 
													<div class="chaxun_input_div">
														<button type="submit" class="btn btn-primary">查询</button>
													</div> 
										        </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="box border green" style="padding: 0px 0px 1% 0px;">
									<div class="box-title">
										<h4>
											<i class="fa fa-table"></i>理赔查询
										</h4>
									</div>
									<div class="box-body">
										<table id="datatable" style="font-size: 13px;" cellpadding="0"
											cellspacing="0" border="0"
											class="datatable table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th class="center">索赔编号</th>
													<th class="center">被保险人名称</th>
													<th class="center">被保险人证件号</th>
													<th class="center">被保险人医保卡号</th>
													<th class="center">理赔处理状态</th>
													<th class="center">理赔详情</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${pageResult.pageList }" var="item">
													<tr>
														<td class="center">${item.caseId }</td>
														<td class="center">${item.insuName }</td>
														<td class="center">${item.idCard }</td>
														<td class="center">${item.miCardId }</td>
														<td class="center">${item.settlementStatus }</td>
														<td class="center hidden-xs"><a href="javascript:showSettlementInfo('${item.caseId}','${item.insuName}','${item.idCard}','${item.miCardId}');">查看详情</a></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<div id="paging1" class="page" style="margin-top: 1%"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Edit Modal -->
	<div class="modal fade" id="showSettlementModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel"></div>
	<div class="modal fade" id="showBillInfoModal" tabindex="-1" role="dialog"
  		aria-labelledby="myModalLabel2"></div>	
	<div class="modal fade" id="showIllegalInfoModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel3"></div>	
</body>
<script type="text/javascript" charset="utf-8">
	$(function() {
		 $('#fpxqff').mouseover(function(){
	   		 $(this).css("color","#515151");
	   	 }); 
	   	 $('#fpxqff').mouseout(function(){
	   		 $(this).css("color","#7e7e7e");
	   	 });
		$("#paging1")
				.pagination(
						{
							items : parseInt('${pageResult.pages}'),
							currentPage : parseInt('${pageResult.pageNum}'),
							cssStyle : "light-theme",
							prevText : "上一页",
							nextText : "下一页",
							hrefTextPrefix : "${ROOT}/settlement/settlementList.html?pageSize=${pageResult.pageSize}&total=${pageResult.total}&pageNum="
						});
		
		$("a.collapse").click(function(){
			var $parentDiv = $(this).parent().parent();
			if($(this).children("i").hasClass("fa-chevron-down")){
				$(this).children("i").removeClass("fa-chevron-down").addClass("fa-chevron-up");
				$parentDiv.next().show();
				$parentDiv.parent().prev().hide();
			}else{
				$(this).children("i").removeClass("fa-chevron-up").addClass("fa-chevron-down");
				$parentDiv.next().hide();
				$parentDiv.parent().prev().show();
			}
		});
		
	});
	
	
	function showSettlementInfo(caseId,insuName,idCard,miCardId){
		$('#showSettlementModal').modal('show');
		$md = $('#showSettlementModal');
		$.ajax({
			type : "POST",
			url : "${ROOT}/settlement/getSettlementInfo/"+caseId,
			data:{caseId:caseId,insuName:insuName,idCard:idCard,miCardId:miCardId},
			dataType : "html",
			success : function(data) {
				$md.html(data);
				$md.modal();
			},
			error : function(request) {
				alert('连接失败，请稍后再试。');
			}
		}); 
	}
	
</script>
</html>
