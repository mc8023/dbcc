<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set scope="request" var="ROOT" value="http://${header.Host}${pageContext.request.contextPath}" />
<div class="modal-dialog" role="document">
			<div class="modal-content bill_info_div">
				<div class="modal-header">
					<!-- <button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button> -->
				<span class="icono-cross" id="FPXQ" style="display:inline-block;float:right;margin-top:-5px;color:#7e7e7e;"/>

					<h4 class="modal-title" id="myModalLabel2">发票详情</h4>
				</div>
				<form action="">
				<input type="hidden" id="billinfo_caseid" value="${billsInfoDto.caseId}"/>
				 <div class="box-body">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th>发票号</th>
										<th>就诊日期</th>
										<th>就诊医疗机构</th>
										<th>票面金额</th>
										<th>第三方支付金额</th>
										<th>申请金额</th>
										<th>核减金额</th>
										<th>核减明细</th>
									  </tr>
									</thead>
									<tbody>
									<c:forEach items="${pageResult.pageList }" var="item">
											<tr>
												<td>${item.billCode }</td>
												<td>${item.clinDate }</td>
												<td title="${item.hosptailName }" class="product-buyer-name">${item.hosptailName }</td>
												<td>${item.totalCost }</td>
												<td>${item.thirdPay }</td>
												<td>${item.totalCost }</td>
												<td>${item.cutPay }</td>
												<td>
												<c:if test="${item.billId!=null}"> 
												<a href="javascript:showIllegalInfo('${item.billId }','${item.billCode }')" >详情</a>
												</c:if>
												<c:if test="${item.billId==null}"> 
												${item.resudEsc}
												</c:if>
												</td>
											</tr>
									</c:forEach>
									</tbody>
								  </table>
							</div>
					<div class="modal-footer">
	                  <div id="paging2" class="page" style="margin-top: 1%"></div>
					</div>
				</form>
			</div>
		</div>
		 <script type="text/javascript" charset="utf-8">
		$(function() {
			$("#paging2")
					.pagination(
							{
								items : parseInt('${pageResult.pages}'),
								currentPage : parseInt('${pageResult.pageNum}'),
								cssStyle : "light-theme",
								prevText : "上一页",
								nextText : "下一页",
								onPageClick : changePage
							});
			
			
		});
		function changePage(){
			showBillsInfo($("#billinfo_caseid"),$("#paging2").pagination('getCurrentPage'));
		}
		
		function showIllegalInfo(billId,billCode,pageNum){
	    	$md = $('#showIllegalInfoModal');
	  		$.ajax({
	  			type : "POST",
	  			url : "${ROOT}/settlement/showIllegalsInfo/"+billId,
	  			data:{billId:billId,billCode:billCode,pageNum:pageNum},
	  			dataType : "html",
	  			success : function(data) {
	  				$md.html(data);
	  				$md.modal();
	  			},
	  			error : function(request) {
	  				alert('连接失败，请稍后再试。');
	  			}
	  		}); 
	      }
		$(function(){
			$('#FPXQ').mouseover(function(){
	    		 $(this).css("color","#515151");
	    	 }); 
	    	 $('#FPXQ').mouseout(function(){
	    		 $(this).css("color","#7e7e7e");
	    	 });
	    	 $('#FPXQ').click(function(){
	    		 $('#showBillInfoModal').modal('hide');
	    	 });
	         $('#showBillInfoModal').modal({backdrop: 'static', keyboard: false});

	      });
		</script>