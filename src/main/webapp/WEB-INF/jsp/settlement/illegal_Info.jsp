<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set scope="request" var="ROOT" value="http://${header.Host}${pageContext.request.contextPath}" />
<div class="modal-dialog" role="document">
			<div class="modal-content illegal_info_div">
				<div class="modal-header">
					<!-- <button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button> -->
					<span class="icono-cross" id="FPBH" style="display:inline-block;float:right;margin-top:-5px;color:#7e7e7e;"/>
					<h4 class="modal-title" id="myModalLabel3">发票编号(${illegalInfoDto.billCode})</h4>
				</div>
				<form action="">
				<input type="hidden" id="illegalinfo_billcode" value="${illegalInfoDto.billCode}"/>
				<input type="hidden" id="illegalinfo_billid" value="${illegalInfoDto.billId}"/>
				 <div class="box-body">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th>核减项目</th>
										<th>违规类别</th>
										<th>核减原因</th>
										<th>核减金额</th>
									  </tr>
									</thead>
									<tbody>
									<c:forEach items="${pagelist}" var="item">
											<tr>
												<td>${item.xmmc }</td>
												<td>${item.type }</td>
												<td>${item.deductionReason }</td>
												<td>${item.cutAmount }</td>
												<td></td>
											</tr>
									</c:forEach>
									</tbody>
								  </table>
							</div>
					<div class="modal-footer">
	                  <div id="paging3" class="page" style="margin-top: 1%"></div>
					</div>
				</form>
			</div>
		</div>
		 <script type="text/javascript" charset="utf-8">
		
		function changePage3(){
			showBillsInfo($("#illegalinfo_billid"),$("#illegalinfo_billcode"),$("#paging3").pagination('getCurrentPage'));
		}
		$(function(){
			 $('#FPBH').mouseover(function(){
	    		 $(this).css("color","#515151");
	    	 }); 
	    	 $('#FPBH').mouseout(function(){
	    		 $(this).css("color","#7e7e7e");
	    	 });
	    	 $('#FPBH').click(function(){
	    		 $('#showIllegalInfoModal').modal('hide');
	    	 });
	         $('#showIllegalInfoModal').modal({backdrop: 'static', keyboard: false});

	      });
		
		</script>