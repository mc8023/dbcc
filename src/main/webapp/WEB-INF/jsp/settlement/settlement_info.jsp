<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set scope="request" var="ROOT" value="http://${header.Host}${pageContext.request.contextPath}" />
<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<!-- <button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button> -->
					<span class="icono-cross" id="LPJAS" style="display:inline-block;float:right;margin-top:-5px;color:#7e7e7e;" />
					<h4 class="modal-title" id="myModalLabel">理赔结案书</h4>
				</div>
				<form action="" id="settlementInfoForm"
					class="form-inline" role="form">
					<input type="hidden" id="nd" value="${settlementInfo.MED_YEAR}">
					<input type="hidden" id="insu_id" value="${settlementInfo.INSU_ID}">
					<div id="tips" align="center"></div>
					<div class="modal-body">
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label ">索赔编号</label>
							<p class="control_p col-sm-3 control-label" ><input type="hidden" id="case_id" value="${settlementInfo.CASE_ID}">${settlementInfo.CASE_ID}</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label">开户银行</label>
							<p class="control_p col-sm-3 control-label">${settlementInfo.BANK_NAME}</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label ">被保险人姓名</label>
							<p class="control_p col-sm-3 control-label" >${settlementInfo.insuName}</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label">银行账户名</label>
							<p class="control_p col-sm-3 control-label">${settlementInfo.BANK_USER_NM}</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label ">被保险人证件号</label>
							<p class="control_p col-sm-3 control-label" >${settlementInfo.idCard}</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label">银行账户</label>
							<p class="control_p col-sm-3 control-label">${settlementInfo.BANK_ACCO_ID}</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label ">被保人人员类别</label>
							<p class="control_p col-sm-3 control-label" >
							${settlementInfo.ITEM_NM}
							</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label">被保人医保号</label>
							<p class="control_p col-sm-3 control-label">${settlementInfo.miCardId}</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label ">第三方支付金额</label>
							<p class="control_p col-sm-3 control-label" >${settlementInfo.THIRD_PAY}</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label">票面金额</label>
							<p class="control_p col-sm-3 control-label">${settlementInfo.BILL_TOTAL}</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label ">核减金额</label>
							<p class="control_p col-sm-3 control-label" >${settlementInfo.CUT_PAY}</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label">申请金额</label>
							<p class="control_p col-sm-3 control-label">${settlementInfo.BILL_TOTAL}</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label ">赔付比例</label>
							<p class="control_p col-sm-3 control-label" >${settlementInfo.COMP_RATE}</p>
						</div>
<!-- 						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label">免赔金额</label>
							<p class="control_p col-sm-3 control-label"></p>
						</div> -->
							<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label ">收集日期</label>
							<p class="control_p col-sm-3 control-label" >${settlementInfo.RECE_DATE}</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label">赔付金额</label>
							<p class="control_p col-sm-3 control-label">${settlementInfo.COMP_PAY}</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label ">结案日期</label>
							<p class="control_p col-sm-3 control-label" >${settlementInfo.CLOSE_DATE}</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label">超保额扣减金额</label>
							<p class="control_p col-sm-3 control-label">${settlementInfo.EXCE_PRE}</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label ">保单类型</label>
							<p class="control_p col-sm-3 control-label" >
							<c:if test="${settlementInfo.ITEM_NM=='员工'}">
   								<c:choose>
									<c:when test="${settlementInfo.PRODUCT_TYPE==1}">
										意外身故
									</c:when>
									<c:when test="${settlementInfo.PRODUCT_TYPE==2}">
										意外伤残
									</c:when>
									<c:otherwise>
										意外医疗
									</c:otherwise>
								</c:choose>
							</c:if>
							<c:if test="${settlementInfo.ITEM_NM=='子女'}">
										疾病
							</c:if>
							<c:if test="${settlementInfo.ITEM_NM=='供养'}">
										疾病
							</c:if>
							</p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label">发票详情</label>
							<p class="control_p col-sm-3 control-label"><a href="javascript:showBillsInfo()">详细</a></p>
						</div>
						<div class="form-group">
							<label class="control_label_zz col-sm-3 control-label ">理赔结案书</label>
							<p class="control_p col-sm-3 control-label" >
								<!-- <button type="button" class="btn btn-primary-xz"  id="exportData" >下载</button> -->
								<a href="javascript:exportData()">下载</a>
								<%-- <a href="${ROOT}/settlement/downloadClaimPdf/${settlementInfo.CASE_ID}?insu_id=${settlementInfo.INSU_ID}&med_year=${settlementInfo.MED_YEAR}">下载</a> --%>
							</p>
						</div>
					</div>
					<!-- <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					</div> -->
				</form>
			</div>
		</div>
      <script type="text/javascript" charset="utf-8">
      function exportData(){
    	  var insu_id=$("#insu_id").val();
    	  var med_year=$("#nd").val();
    	  var caseId=$("#case_id").val();
    	  	/* 采用post提交方式 */
    		var link="${ROOT}/settlement/downloadClaimPdf/"+caseId+"?";
    		/* 首先创建一个form表单   */
    		var tempForm = document.createElement("form");    
    		tempForm.id="tempForm1";
    		/* 制定发送请求的方式为post   */
    		tempForm.method="post";   
    		/* 此为window.open的url，通过表单的action来实现   */
    		tempForm.action=link;  
    		/* 利用表单的target属性来绑定window.open的一些参数（如设置窗体属性的参数等） */  
    		tempForm.target="_parent";
    		/* 创建input标签，用来设置参数   */
    	    var hideInput1 = document.createElement("input");    
    	    hideInput1.type="hidden";    
    	    hideInput1.name= "insu_id";  
    	    hideInput1.value= insu_id;
    	    /* 创建input标签，用来设置参数   */
    	    var hideInput2 = document.createElement("input");    
    	    hideInput2.type="hidden";    
    	    hideInput2.name= "med_year";  
    	    hideInput2.value= med_year;
    	    /*  将input表单放到form表单里   */
    	    tempForm.appendChild(hideInput1); 
    	    tempForm.appendChild(hideInput2); 
    	    /* 将此form表单添加到页面主体body中   */
    	    document.body.appendChild(tempForm); 
    		/* 手动触发，提交表单   */
    	    tempForm.submit();
    		/* 从body中移除form表单 */  
    	    document.body.removeChild(tempForm);  
      }
      
      function showBillsInfo(pageNum){
    	var caseId=$("#case_id").val();
    	$md = $('#showBillInfoModal');
  		$.ajax({
  			type : "POST",
  			url : "${ROOT}/settlement/showBillsInfo/"+caseId,
  			data:{caseId:caseId,pageNum:pageNum},
  			dataType : "html",
  			success : function(data) {
  				$md.html(data);
  				$md.modal();
  			},
  			error : function(request) {
  				alert('连接失败，请稍后再试。');
  			}
  		}); 
      }
      $(function(){
    	 $('#LPJAS').mouseover(function(){
    		 $(this).css("color","#515151");
    	 }); 
    	 $('#LPJAS').mouseout(function(){
    		 $(this).css("color","#7e7e7e");
    	 });
    	 $('#LPJAS').click(function(){
    		 $('#showSettlementModal').modal('hide');
    	 });
         $('#showSettlementModal').modal({backdrop: 'static', keyboard: false});
		
      });
      </script>