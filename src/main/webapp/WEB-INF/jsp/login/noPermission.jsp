<%@ page language="java" pageEncoding="UTF-8"%>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Cloud Admin | Login</title>
    <%@include file="/WEB-INF/comm/_env.jsp"%>
</head>
<body>	
	<!-- PAGE -->
	<section id="page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="divide-100"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 not-found text-center" >
				   <div class="error" style="font-size: 78px;right: 30%;">
					  您没有权限访问！
				   </div>
				</div>
			</div>
		</div>
	</section>
	<!-- /JAVASCRIPTS -->
</body>
</html>