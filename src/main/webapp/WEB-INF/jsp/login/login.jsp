<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">

<head>
<!-[if lt IE 9]>
  <script src="http://apps.bdimg.com/libs/html5shiv/3.7/html5shiv.min.js"></script>
  <script src="http://apps.bdimg.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]->
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
<link rel="stylesheet" type="text/css" href="bootstrap.min.css" media="screen"/>
<script type="text/javascript" src="js/respond.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/jquery.placeholder.js"></script>



	<?import namespace="v" implementation="#default#VML" ?>
	<meta http-equiv="content-type X-UA-Compatible" content="text/html; charset=UTF-8;IE=EmulateIE9">
	<meta charset="utf-8">
	<title>Cloud Admin | Login</title>
    <%@include file="/WEB-INF/comm/_env.jsp"%>
    
</head>


<!-- <body class="login" style="background:url(bootstrap_resource/img/homepage.png) no-repeat; background-size:100%;">	 -->
<body class="login" style="background:url(bootstrap_resource/img/homepage.jpg) no-repeat; background-size:100%;margin-top:-1px;">
	<!-- PAGE -->
	<section id="page">
			<!-- HEADER -->
			<header>
				<!-- NAV-BAR -->
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
						</div>
					</div>
				</div>
				<!--/NAV-BAR -->
			</header>
			<!--/HEADER -->
	 </section>		
			<!-- LOGIN -->
			 <!--   <img src="./bootstrap_resource/img/index2.png" style="margin-top:10%;"> -->
			
		    <div  style="position: absolute;left: 32%;top: 25%;">
			  <section id="login" class="visible">
				<div class="container" class="pageLeft40">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<div class="login-box-plain-mod" style="width:450px;height:400px;">
							  <label style="font-size:25px;margin-bottom:30px;margin-left:10%;color: white;""><img src="./bootstrap_resource/img/logo-freebsd-devilx32.png" style="margin-right:10px;margin-top: -7px;">DB管理系统</label>
								<form id="form" role="form"  method="post"  action="${ROOT}/login" style="padding: 15px 160px 15px 10px;">
								  <div class="form-group">
									<!-- <label for="exampleInputEmail1">用户名</label>  -->
									<i class="icon-user"></i>
									<input type="text" name="username" class="form-control username user" id="username" value="${username}" placeholder="账号" onkeydown="javascript:if(event.keyCode==13) login();">
								  </div>
								  <div class="form-group"> 
									<!-- <label for="exampleInputPassword1">密码</label> -->
									<i class="icon-pwd"></i>
									<input type="password" class="form-control password pwd" name="password" id="password" value="${password}" placeholder="密码"  onkeydown="javascript:if(event.keyCode==13) login();">
								  </div>
								  <div class="form-actions-mod">
								    <input type="hidden" id="errorMessageBack" value="${errorMessage}"/>
									<label id="errorMessage" style="color: red;"></label>
									<button type="button" id="okbutton" class="btn btn-danger"  onclick="llq()" style="width:280px" >登录</button>
								  </div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
			 
			 
			</div>
			
			
	
	<script>
	
	function llq(){
		var b_name = navigator.appName;
	    var b_version = navigator.appVersion;
	    var version = b_version.split(";");
	    var trim_version = version[1].replace(/[ ]/g, "");
	    if (b_name == "Microsoft Internet Explorer") {
	        /*如果是IE6或者IE7或者IE8*/
	        if (trim_version == "MSIE7.0" || trim_version == "MSIE6.0" || trim_version == "MSIE8.0" ) {
	            alert("IE浏览器版本过低，请更新IE浏览器或使用其他浏览器");
	            //然后跳到需要连接的下载网站 
	            window.location.href="http://jiaoxueyun.com/download.jsp"; 
	        }
	    }

	}
	
		jQuery(document).ready(function() {		
			App.setPage("login");  //Set current page
			App.init(); //Initialise plugins and elements
		});
		
		
		 <%--  <%
	        String errorMessage = (String)request.getAttribute("errorMessage");
	        if(errorMessage!=null && errorMessage.length() > 0) {
	            out.println("onLoginError();");
	        }
	    %> --%>
	  $(function(){
	    	if($("#errorMessageBack").val()!=""){
	    		$("#errorMessage").text("*用户名或密码不正确！");
	    	}
	    });

	  $(function(){
			if($('#header', parent.document).length!=0){
				window.parent.location="${ROOT}/login";
				$.cookie('loginInfo','未登陆或登陆失效!');
			}else{
				infoActivate();
			}
	    });
	    function infoActivate(){
	    	var loginInfo=$.cookie('loginInfo');
	        if(loginInfo){
	        	$('#errorMessage').text(loginInfo);
	        }
	        $.cookie('loginInfo','');
	        $.cookie('loginUser','');
	        $.cookie('*.session.id','');
	    }

	    $(function () {
	        //登录按钮点击事件
	      /*   $("#form").submit(function (e) {
	           alert("1111");
	        }); */
	        $("#okbutton").click(function(){
	        	/* if($("input[name='rememberMe']").parent().hasClass("checked")){
	        		$("input[name='rememberMe']").val("true");
	        	} */
	        	$("#form").submit();
	        	
	        });

	    });
	    
	    function login(){
	    	 $("#okbutton").click();
	    	
	    }
	</script>
	
</body>
</html>