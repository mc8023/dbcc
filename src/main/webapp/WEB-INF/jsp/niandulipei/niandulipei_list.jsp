<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@include file="/WEB-INF/comm/_env.jsp"%>
<title>东风 | 菜单管理</title>
</head>
<body onload="javascript:load();">
	<div id="main-content">
		<div class="container">
			<div class="row">
				<div id="content" class="col-lg-12">
					<!-- PAGE HEADER-->
					<div class="row">
						<div class="col-sm-12">
							<div class="page-header">
								<ul class="breadcrumb"></ul>
								<form id="niandulipeiForm" action="${ROOT}/niandulipei/niandulipeiList.html" method="post" class="form-inline">
							            <input type="hidden" id="pleve" name="pleve" value="${plevl}"/>
										<input type="hidden" id="pcon" name="pcon" value="${qcon}"/>
							             <div class="form-group" style="margin-left:50px;">
											  <label class="control-label info">年度:</label> 
											  </div>
												<div class="form-group">
													<div class="date_input_div_begin">
													<input type="hidden" id="checkdate" value="${pstardate}"/>
													    <input name="nd" id="nd" type="text" placeholder="选择年份" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({dateFmt:'yyyy',minDate:'2000y',maxDate:'%y'})"value="${xzndhidden}"/>
										            </div> 
										        </div>
							             <div class="form-group" style="margin-left:10px;" id="bkdiv" >
											<div class="list" id="list" style="padding:0px; margin:0px;">
								                <label class="control-label info">板块:</label>
								                <input type="hidden" id="bblock" value="${pbkhidden}"/>
											    <input type="text" class="form-control" readonly="true" id="bk" style="width:150px;" value="${pbkhidden11}">
											    <input type="hidden" class="form-control" name="bk" style="width:150px;" value="${pbkhidden}">
											    <div class="select" id="select" style="border: 1px solid #CCCCCC; display: none; background: #EEEEEE; position: fixed; z-index: 999; min-width: 150px;margin-left:39px;">
											        <ul style="padding-left:15px;">
											        	<c:forEach items="${listBk }" var="item">
												            <li style="list-style-type:none;">
												            <input type="checkbox" value="${item.common1 }" 
												            name="check" class="chk"/>${item.common2 }</li>
											        	</c:forEach>
											        </ul>
											   </div>
							            </div>
							            </div>
							            		<div class="form-group" style="margin-left:10px;">
													<label class="control-label info">理赔类型:</label> 
													<input type="hidden" id="lplxs" name="lplxs" value="${lplxhidden}"></input>
													<select id="lplx" name="lplx" class="form-control" style="width:150px;text-align:center;">
													<option value="-1" >全部</option>
													<option ${lplxhidden == '1' ? "selected='selected'" : ""} value="1">线下理赔</option>
													<option ${lplxhidden == '2' ? "selected='selected'" : ""} value="2">医院直赔</option>
													</select>
												</div>
										<div class="form-group">
							                <button type="submit" class="btn btn-primary-cx">查询 </button> 
							                <button type="button" class="btn btn-primary-xz"  id="exportData" >导出 </button>
							                <!-- <a type="button" class="btn btn-primary-xz" id="exportData">导出</a> -->
							        </div>
								</form>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="box border green" style="padding: 0px 0px 1% 0px;">
								<div class="box-title">
									<h4>
										<i class="fa fa-table"></i>年度理赔台账信息
									</h4>
								</div>
								<div class="clear"></div>
								<div class="box-body" style="overflow-x: auto; overflow-y: auto;"> 
									<table id="niandulipeiTable" style="width:2000px;" class="datatable table table-striped table-bordered table-condensed table-hover">
										<thead>
											<tr>
												<th class="table_th_style" rowspan="3">
													<div class="table_th_div_style">序号 </div>
												</th>
												<th class="table_th_style" rowspan="3">
													<div class="table_th_div_style">单位名称 </div>
												</th>
												<th class="table_th_style" rowspan="3">
													<div class="table_th_div_style" >合计(元)</div>
												</th>
												<th class="table_th_style" colspan="12">
													<div class="table_th_div_style">案件理赔 </div>
												</th>
												<th class="table_th_style" rowspan="3">
													<div class="table_th_div_style">理赔类型 </div>
												</th>
											</tr>
											<tr>
												<th class="table_th_style" >
													<div class="table_th_div_style">1月</div>
												</th>
												<th class="table_th_style" >
													<div class="table_th_div_style">2月</div>
												</th>
												<th class="table_th_style" >
													<div class="table_th_div_style">3月</div>
												</th>
												<th class="table_th_style" >
													<div class="table_th_div_style">4月</div>
												</th>
												<th class="table_th_style" >
													<div class="table_th_div_style">5月</div>
												</th>
												<th class="table_th_style" >
													<div class="table_th_div_style">6月</div>
												</th>
												<th class="table_th_style" >
													<div class="table_th_div_style">7月</div>
												</th>
												<th class="table_th_style">
													<div class="table_th_div_style">8月</div>
												</th>
												<th class="table_th_style" >
													<div class="table_th_div_style">9月</div>
												</th>
												<th class="table_th_style" >
													<div class="table_th_div_style">10月</div>
												</th>
												<th class="table_th_style" >
													<div class="table_th_div_style">11月</div>
												</th>
												<th class="table_th_style" >
													<div class="table_th_div_style">12月</div>
												</th>
											</tr>
											<tr>
												<th class="table_th_style">
													<div class="table_th_div_style">理赔金额(元)</div>
												</th>
												<th class="table_th_style">
													<div class="table_th_div_style">理赔金额(元)</div>
												</th>
												<th class="table_th_style">
													<div class="table_th_div_style">理赔金额(元)</div>
												</th>
												<th class="table_th_style"> 
													<div class="table_th_div_style">理赔金额(元)</div>
												</th>
												<th class="table_th_style">
													<div class="table_th_div_style">理赔金额(元)</div>
												</th>
												<th class="table_th_style">
													<div class="table_th_div_style">理赔金额(元)</div>
												</th>
												<th class="table_th_style">
													<div class="table_th_div_style">理赔金额(元)</div>
												</th>
												<th class="table_th_style">
													<div class="table_th_div_style">理赔金额(元)</div>
												</th>
												<th class="table_th_style">
													<div class="table_th_div_style">理赔金额(元)</div>
												</th>
												<th class="table_th_style">
													<div class="table_th_div_style">理赔金额(元)</div>
												</th>
												<th class="table_th_style">
													<div class="table_th_div_style">理赔金额(元)</div>
												</th>
												<th class="table_th_style">
													<div class="table_th_div_style">理赔金额(元)</div>
												</th>
											</tr>
										</thead>
										
										<tbody>
											<c:forEach items="${listInfo }" var="item">
												<tr>
													<td >${item.common1} </td>
													<td >${item.common3} </td>
													<td class="center">${item.common7} </td>
													<td class="center">${item.common9} </td>
													<td class="center">${item.common11} </td>
													<td class="center">${item.common13} </td>
													<td class="center">${item.common15} </td>
													<td class="center">${item.common17} </td>
													<td class="center">${item.common19} </td>
													<td class="center">${item.common21} </td>
													<td class="center">${item.common23} </td>
													<td class="center">${item.common25} </td>
													<td class="center">${item.common27} </td>
													<td class="center">${item.common29} </td>
													<td class="center">${item.common31} </td>
													<td class="center">${item.lplx } </td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
									<div id="paging1" class="page" style="margin-top: 1%"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript" charset="utf-8">
$(function(){
    //设置年份的选择 (上下10年)
	var myDate= new Date(); 
	//起始年份  
	var startYear=myDate.getFullYear()-10;
	//结束年份 
	var endYear=myDate.getFullYear()+10;
 //   for (var i=startYear;i<=endYear;i++) {
    		//$("#nd").append("<option value='"+i+"'>"+i+"</option>");    		
	//}
	//$("#nd").val(myDate.getFullYear());
	
	//设置板块的选择
	var bk = $("#bk");
    bk.click(function(){
        $(".select").slideToggle("slow");
    });
    
    //下拉框的高度大于180px时，显示滚动条，反之隐藏
    var iTarget = 150;
    var sel_h = $("#select").height();
    if(sel_h > iTarget) {
        $("#select").css({
            "height" : "150px",
            "overflow-y" : "scroll"
        }); 
    }
});
//点击其他地方隐藏
$(document).bind("click",function(event){ 
    var e = event || window.event;   
    var elem = e.srcElement||e.target;   
    while(elem)   
    {   
        if(elem.id == "list")   
        {   
            return;   
        }   
        elem = elem.parentNode;        
    }   
   ensure();
   $('#select').hide();  
});
  
//判断是否选中checkbox的值
function ensure() {
	var resultText = [];
	var resultValue = [];
	
	var check_array = document.getElementsByName("check");
	for(i in check_array) {
	    if(check_array[i].checked) {
	    	resultText.push(check_array[i].nextSibling.nodeValue);
	    	resultValue.push(check_array[i].value);
	    }
	}
    $("#bk").val(resultText);
    $("[name='bk']").val(resultValue);
    $("#select").css("display","none");
}

//导出
$("#exportData").click(function(){
	var nd = $("[name='nd']").val();
	var bk = $("[name='bk']").val();
	var lplxs =$("[name='lplx']").val();
	 /* 采用post提交方式 */
	var link="${ROOT}/niandulipei/download?";
	/* 首先创建一个form表单   */
	var tempForm = document.createElement("form");    
	tempForm.id="tempForm1";
	/* 制定发送请求的方式为post   */
	tempForm.method="post";   
	/* 此为window.open的url，通过表单的action来实现   */
	tempForm.action=link;  
	/* 利用表单的target属性来绑定window.open的一些参数（如设置窗体属性的参数等） */  
	tempForm.target="_parent";
	/* 创建input标签，用来设置参数   */
    var hideInput1 = document.createElement("input");    
    hideInput1.type="hidden";    
    hideInput1.name= "nd";  
    hideInput1.value= nd;
    /* 创建input标签，用来设置参数   */
    var hideInput2 = document.createElement("input");    
    hideInput2.type="hidden";    
    hideInput2.name= "bk";  
    hideInput2.value= bk;
    /* 创建input标签，用来设置参数   */
    var hideInput3 = document.createElement("input");    
    hideInput3.type="hidden";    
    hideInput3.name= "lplx";  
    hideInput3.value= lplxs;
    /*  将input表单放到form表单里   */
    tempForm.appendChild(hideInput1); 
	tempForm.appendChild(hideInput2); 
	tempForm.appendChild(hideInput3); 
    /* 将此form表单添加到页面主体body中   */
    document.body.appendChild(tempForm); 
	/* 手动触发，提交表单   */
    tempForm.submit();
	/* 从body中移除form表单 */  
    document.body.removeChild(tempForm);  
	/* var url="${ROOT}/niandulipei/download?nd="+nd+"&bk="+bk+"&lplx="+lplxs; 
	$("#exportData").attr("href",url);  */
});

$(function(){
	if($("#pleve").val()!=null && $("#pleve").val()!=""){
		if($("#pleve").val()=="2"||$("#pleve").val()=="3"||$("#pleve").val()=="4"){
			document.getElementById("bkdiv").style.display="none";//隐藏
		}
	}
	
});
function load(){
	var bkval = new Array;
	
	var bk = '<%=request.getAttribute("pbkhidden") %>';
	bkval = bk.split(",");
	for(var j=0;j<bkval.length;j++){
	    $("input[name='check']").each(function(i){
			if($(this).val()==bkval[j]){
				$(this).prop("checked", true);
			
			}
	    });}
	var resultText = [];
	var check_array = document.getElementsByName("check");
	for(i in check_array) {
	    if(check_array[i].checked) {
	    	resultText.push(check_array[i].nextSibling.nodeValue);
	    }
	}
    $("#bk").val(resultText);
};
</script>
</html>
