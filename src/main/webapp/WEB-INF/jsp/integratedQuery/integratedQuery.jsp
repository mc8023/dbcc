<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@include file="/WEB-INF/comm/_env.jsp"%>
<title>东风 | 菜单管理</title>
</head>
<body>
			<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<ul class="breadcrumb">
									</ul>
										<form class="form-inline" id="form" role="form" action="${ROOT}/integratedQuery/integratedQuery.html">
										<input type="hidden" id="zhcx" name="zhcx" value="${zhcx}"/>
										<div class="form-group col-sm-12" align="right">
										
										<div class="col-sm-4 ">
											<label>索赔编号</label> 
											<input type="text" class="form-control" style="width:200px" id="caseId" name="caseId" placeholder="索赔编号" value="${case_id}">
										</div> 
											
										<div class="col-sm-4 ">
											<label >投保人姓名</label> 
												<input type="text" class="form-control" style="width:200px" name="tbrxm" id="tbrxm" placeholder="投保人姓名" value="${tbrxm}">
										</div>
										
										<div class="col-sm-4 ">
											<label >员工编号</label> 
												<input
													type="text" class="form-control" name="ygbh" style="width:200px"
													id="ygbh" placeholder="员工编号" value="${ygbh}">
										</div>
										
										</div>
										
										 <div class="form-group col-sm-12" align="right" style="margin-top:10px">
											  <div class="col-sm-4 ">
											        <label >投保年度</label> 
													
														<input	type="text" class="form-control" style="width:200px" name="nd" id="nd" placeholder="年度" value="${nd}">
											</div> 
											
										
										<div class="col-sm-4 ">
											<label >被保人姓名</label> 
												<input type="text" class="form-control" style="width:200px" name="bbrxm" id="bbrxm" placeholder="被保险人姓名" value="${bbrxm}">
										</div>
											  <div class="col-sm-4 ">
   									  <label >单位名称</label> 
										
											<input
													type="text" class="form-control" name="dwmc" style="width:200px"
													id="dwmc" placeholder="单位名称" value="${dwmc}">
										</div>
										</div>
										
										
								
   									  <div class="form-group col-sm-12 " align="right" style="margin-top:10px">
   									  <div class="col-sm-4 ">
											<label >被保人身份证</label> 
												<input
													type="text" class="form-control" name="bbrsfz" style="width:200px"
													id="bbrsfz" placeholder="被保险人身份证号" value="${bbxrsfz}">
										</div>
   									 <div class="col-sm-6 ">
   									 <div class="form-group">
													<label class="sr-only"></label> 
													<div class="button_a_div">
														<button type="submit" class="btn btn-primary-cx" >查询</button>
														<select class="form-control" name="dcInfo" id="dcInfo" value="${dcInfo}">
																<option ${dcInfo == '1' ? "selected='selected'" : ""} value="1">投保信息</option>
																<option ${dcInfo == '2' ? "selected='selected'" : ""} value="2">理赔信息</option>
														</select>	
														<button type="button" class="btn btn-primary-xz" onClick="daochuInfo()" id="daochu" >导出 </button>
														<!-- <a type="button"  class="btn btn-primary-xz" id="daochu" onclick="daochuInfo();" href="javascript:void(0);">导出</a> -->
													</div> 
												</div>
   									 
   									 </div>
											 
										</div>
										<!-- <div class="form-group">
									        <label class="sr-only"></label> 
											<div class="chaxun_input_div">
												<input
													type="text" class="form-control" name="jzyy"
													id="jzyy" placeholder="就诊医院.....">
											</div> 
										</div>
										<div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_begin">
													    <input name="jzrqf" id="jzrqf" type="text" placeholder="就诊日期  自" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'jzrqt\')}'})"/>
										            </div> 
										        </div>
										        <div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_end">
													    <input name="jzrqt" id="jzrqt" type="text" placeholder="至" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'jzrqf\')}'})"/>
										            </div> 
										        </div>
										        <div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_begin">
													    <input name="jsrqf" id="jsrqf" type="text" placeholder="结算日期  自" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'jsrqt\')}'})"/>
										            </div> 
										        </div>
										        <div class="form-group">
													<label class="sr-only"></label> 
													<div class="date_input_div_end">
													    <input name="jsrqt" id="jsrqt" type="text" placeholder="至" class="Wdate form-control" style="height:34px" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'jsrqf\')}'})"/>
										            </div> 
										        </div> -->
										       
										     	
										
								
											<div class="box">
											
											<div class="box-body font-400" style="display: none;">
	
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="box border green" style="padding: 0px 0px 1% 0px;">
									<div class="box-title">
										<h4>
											<i class="fa fa-table"></i><lable>综合查询</lable>
										</h4>
										<lable style="float:right;" id="total">总条数：${total}</lable>
									</div>
									<div class="box-body" style="overflow-x: auto; overflow-y: auto;">
										<table id="datatable" style=" overflow-x: auto; overflow-y: auto;font-size: 12px;" cellpadding="0"
											cellspacing="0" border="0"
											class="datatable table table-striped table-bordered table-hover">
											<thead>
												<tr>
												    <th class="center"><input type="checkbox" name="allck" value="" ></th>
													<th class="center">索赔编号</th>
													<th class="center" style="width:60px">投保人姓名</th>
													<th class="center">投保类型</th>
													<th class="center" style="width:60px">被保人姓名</th>
													<th class="center">工作单位</th>
													<th class="center">员工编码</th>
													<th class="center" >身份证号</th>
													<th class="center" style="width:160px">投保周期</th>
													<th class="center" style="width:50px">类型</th>
													<th class="center" style="width:80px">详细信息</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${pageResult.pageList }" var="item">
													<tr>
													    <td class="center"><input type="checkbox" value="${item.INSU_ID}-${item.MED_YEAR}" name="integrateCheckbox"></td>
														<td class="center">${item.CASEID}</td>
														<td class="center">${item.POLI_HOLD_NM}</td>
														<td class="center">${item.INSU_TYPE}</td>
														<td class="center" style="white-space:nowrap;">${item.INSU_NM}</td>
														<td class="center">${item.INSU_ORG_NM}</td>
														<%-- <td class="center hidden-xs"><a href="#" onclick="showSettlementInfo('${item.caseId}','${item.insuName}','${item.idCard}','${item.miCardId}');">查看详情</a></td> --%>
													    <td class="center">${item.POLI_HOLD_ID}</td>
													    <td class="center">${item.ID_CARD}</td>
													    <td class="center">${item.POLICY_BEGIN_DT}~${item.POLICY_END_DT}</td>
													    <td class="center">
													    <c:if test="${item.CASEID!=null}">
													  		${item.LPLX}
													    </c:if>
													    </td>
													    <td class="center hidden-xs">
													  	<c:choose>
															<c:when test="${item.CASEID!=null}">
																<a href="javascript:showLpInfo('${item.CASEID}','${item.INSU_ID}'),void(0)">理赔信息</a>
															</c:when>
															<c:otherwise>
																<a href="javascript:showTbInfo('${item.INSU_ID}','${item.MED_YEAR}');">投保信息</a>
															</c:otherwise>
														</c:choose>
														  <%--   <a href="#" onclick="showTbInfo('${item.INSU_ID}','${item.MED_YEAR}');">投保信息</a>
													    <c:if test="${item.CASEID!=null}">
   														  <div style="margin-top:10px"> <a href="#" onclick="showLpInfo('${item.CASEID}','${item.INSU_ID}');">理赔信息</a></div>
														</c:if> --%>
													    </td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<div id="paging2" class="page" style="margin-top: 1%"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Edit Modal -->
	<div class="modal fade" id="showLpInfoModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel"></div>
		<div class="modal fade" id="showTbInfoModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel"></div>

</body>
<script type="text/javascript" charset="utf-8">
	$(function() {
		var paramStr="";
		if($("#dwmc").val()!=""){
			paramStr = "dwmc="+ $("#dwmc").val();
		}
		var paramCaseId="";
		if($("#caseId").val()!=""){
			paramCaseId = "caseId="+ $("#caseId").val();
		}
		var paramTbrxm="";
		if($("#tbrxm").val()!=""){
			paramTbrxm = "tbrxm="+ $("#tbrxm").val();
		}
		var paramBbrxm="";
		if($("#bbrxm").val()!=""){
			paramBbrxm = "bbrxm="+ $("#bbrxm").val();
		}
		var paramBbrsfz="";
		if($("#bbrsfz").val()!=""){
			paramBbrsfz = "bbrsfz="+ $("#bbrsfz").val();
		}
		var paramYgbh="";
		if($("#ygbh").val()!=""){
			paramYgbh = "ygbh="+ $("#ygbh").val();
		}
		var paramNd="";
		if($("#nd").val()!=""){
			paramNd = "nd="+ $("#nd").val();
		}
		var paramDcInfo="";
		if($("#dcInfo").val()!=""){
			paramDcInfo = "dcInfo="+ $("#dcInfo").val();
		}
		var zhcx="";
		if($("#zhcx").val()!=""){
			zhcx = "zhcx="+ $("#zhcx").val();
		}
		
		
		$("#paging2")
				.pagination(
						{
							items : parseInt('${pageResult.pages}'),
							currentPage : parseInt('${pageResult.pageNum}'),
							cssStyle : "light-theme",
							prevText : "上一页",
							nextText : "下一页",
							/* hrefTextPrefix : "${ROOT}/integratedQuery/integratedQuery.html?"+paramDcInfo+"&"+paramStr+"&"+paramCaseId+"&"+zhcx+"&"+paramTbrxm+"&"+paramBbrxm+"&"+paramBbrsfz+"&"+paramYgbh+"&"+paramNd+"&pageSize=${pageResult.pageSize}&total=${pageResult.total}&pageNum=" */
							hrefTextPrefix : "javascript:changeTable(${pageResult.pageSize},${pageResult.total});"
						});
		
		$("a.collapse").click(function(){
			var $parentDiv = $(this).parent().parent();
			if($(this).children("i").hasClass("fa-chevron-down")){
				$(this).children("i").removeClass("fa-chevron-down").addClass("fa-chevron-up");
				$parentDiv.next().show();
				$parentDiv.parent().prev().hide();
			}else{
				$(this).children("i").removeClass("fa-chevron-up").addClass("fa-chevron-down");
				$parentDiv.next().hide();
				$parentDiv.parent().prev().show();
			}
		});
		
	});
	function changeTable(pageSize,total){
        var page_index = $("#paging2").pagination('getCurrentPage');
        var paramStr="";
		if($("#dwmc").val()!=""){
			paramStr = "dwmc="+ $("#dwmc").val();
		}
		var paramCaseId="";
		if($("#caseId").val()!=""){
			paramCaseId = "caseId="+ $("#caseId").val();
		}
		var paramTbrxm="";
		if($("#tbrxm").val()!=""){
			paramTbrxm = "tbrxm="+ $("#tbrxm").val();
		}
		var paramBbrxm="";
		if($("#bbrxm").val()!=""){
			paramBbrxm = "bbrxm="+ $("#bbrxm").val();
		}
		var paramBbrsfz="";
		if($("#bbrsfz").val()!=""){
			paramBbrsfz = "bbrsfz="+ $("#bbrsfz").val();
		}
		var paramYgbh="";
		if($("#ygbh").val()!=""){
			paramYgbh = "ygbh="+ $("#ygbh").val();
		}
		var paramNd="";
		if($("#nd").val()!=""){
			paramNd = "nd="+ $("#nd").val();
		}
		var paramDcInfo="";
		if($("#dcInfo").val()!=""){
			paramDcInfo = "dcInfo="+ $("#dcInfo").val();
		}
		var zhcx="";
		if($("#zhcx").val()!=""){
			zhcx = "zhcx="+ $("#zhcx").val();
		}
        window.location.href ="${ROOT}/integratedQuery/integratedQuery.html?"+paramDcInfo+"&"+paramStr+"&"+paramCaseId+"&"+zhcx+"&"+paramTbrxm+"&"+paramBbrxm+"&"+paramBbrsfz+"&"+paramYgbh+"&"+paramNd+"&pageSize="+pageSize+"&total="+total+"&pageNum="+page_index;
    }
	function showTbInfo(insu_id,nd){
		$md = $('#showTbInfoModal');
		$.ajax({
			type : "POST",
			url : "${ROOT}/baoquan/baoquanInfo",
			data:{nd:nd,insu_id:insu_id},
			dataType : "html",
			success : function(data) {
				$md.html(data);
				$md.modal();
			},
			error : function(request) {
				alert('连接失败，请稍后再试。');
			}
		}); 
	}
	
	function showLpInfo(caseId,insu_id){
		$md = $('#showLpInfoModal');
		$.ajax({
			type : "POST",
			url : "${ROOT}/settlement/settlementList",
			data:{caseId:caseId,insu_id:insu_id},
			dataType : "html",
			success : function(data) {
				$md.html(data);
				$md.modal();
			},
			error : function(request) {
				alert('连接失败，请稍后再试。');
			}
		}); 
	}
	
	function daochuInfo(){
		debugger;
		var dcInfo=$("#dcInfo").val();
		var caseId=$("#caseId").val();
		var tbrxm=$("#tbrxm").val();
		var bbrxm=$("#bbrxm").val();
		var nd=$("#nd").val();
		var bbrsfz=$("#bbrsfz").val();
		var ygbh=$("#ygbh").val();
		var dwmc=$("#dwmc").val();
	
		var boxes = document.getElementsByName("integrateCheckbox"); //找到选项的数组
		 var arrayObj = [];
		    for(i=0;i<boxes.length;i++){
		        if(boxes[i].name=="integrateCheckbox" && boxes[i].checked == true){
		        	arrayObj.push(boxes[i].value);
		        	
		        }
		    }
		    
		   // alert(arrayObj);
		
		
/* 		 if("${pageResult.total}">5000){
			alert("数据量超过5000条,无法导出,请输入查询条件精确查询后导出");
			//Showbo.Msg.alert('数据量超过5000条,无法导出,请输入查询条件精确查询后导出');
			return false;
		}else{ */
			if(dcInfo=="1"){
				 /* 采用post提交方式 */
				var link="${ROOT}/updownload/baoquanxinxi?";
				/* 首先创建一个form表单   */
				var tempForm = document.createElement("form");    
				tempForm.id="tempForm1";
				/* 制定发送请求的方式为post   */
				tempForm.method="post";   
				/* 此为window.open的url，通过表单的action来实现   */
				tempForm.action=link;  
				/* 利用表单的target属性来绑定window.open的一些参数（如设置窗体属性的参数等） */  
				tempForm.target="_parent";
				if(arrayObj!=""){
					/* var url="${ROOT}/updownload/baoquanxinxi?arrayObj="+arrayObj; */
					/* 创建input标签，用来设置参数   */
			        var hideInput = document.createElement("input");    
			        hideInput.type="hidden";    
			        hideInput.name= "arrayObj";  
			        hideInput.value= arrayObj;
			        /*  将input表单放到form表单里   */
			        tempForm.appendChild(hideInput); 
			        /* 将此form表单添加到页面主体body中   */
			        document.body.appendChild(tempForm); 
					/* 手动触发，提交表单   */
			        tempForm.submit();
					/* 从body中移除form表单 */  
			        document.body.removeChild(tempForm);  
				}else{
					/* var url="${ROOT}/updownload/baoquanxinxi?caseId="+caseId+"&tbrxm="+tbrxm+"&bbrxm="+bbrxm+"&nd="+nd+"&bbrsfz="+bbrsfz+"&ygbh="+ygbh+"&dwmc="+dwmc; */ 
					/* 创建input标签，用来设置参数   */
			        var hideInput1 = document.createElement("input");    
			        hideInput1.type="hidden";    
			        hideInput1.name= "caseId";  
			        hideInput1.value= caseId;
			        /* 创建input标签，用来设置参数   */
			        var hideInput2 = document.createElement("input");    
			        hideInput2.type="hidden";    
			        hideInput2.name= "tbrxm";  
			        hideInput2.value= tbrxm;
			        /* 创建input标签，用来设置参数   */
			        var hideInput3 = document.createElement("input");    
			        hideInput3.type="hidden";    
			        hideInput3.name= "bbrxm";  
			        hideInput3.value= bbrxm;
			        /* 创建input标签，用来设置参数   */
			        var hideInput4 = document.createElement("input");    
			        hideInput4.type="hidden";    
			        hideInput4.name= "nd";  
			        hideInput4.value= nd;
			        /* 创建input标签，用来设置参数   */
			        var hideInput5 = document.createElement("input");    
			        hideInput5.type="hidden";    
			        hideInput5.name= "bbrsfz";  
			        hideInput5.value= bbrsfz;
			        /* 创建input标签，用来设置参数   */
			        var hideInput6 = document.createElement("input");    
			        hideInput6.type="hidden";    
			        hideInput6.name= "ygbh";  
			        hideInput6.value= ygbh;
			        /* 创建input标签，用来设置参数   */
			        var hideInput7 = document.createElement("input");    
			        hideInput7.type="hidden";    
			        hideInput7.name= "dwmc";  
			        hideInput7.value= dwmc;
			        /*  将input表单放到form表单里   */
			        tempForm.appendChild(hideInput1); 
			        tempForm.appendChild(hideInput2); 
			        tempForm.appendChild(hideInput3); 
			        tempForm.appendChild(hideInput4); 
			        tempForm.appendChild(hideInput5); 
			        tempForm.appendChild(hideInput6); 
			        tempForm.appendChild(hideInput7); 
			        /* 将此form表单添加到页面主体body中   */
			        document.body.appendChild(tempForm); 
					/* 手动触发，提交表单   */
			        tempForm.submit();
					/* 从body中移除form表单 */  
			        document.body.removeChild(tempForm);  
				}
				//$("#daochu").attr("href",url);
			}else{
				/* 采用post提交方式 */
				var link="${ROOT}/updownload/lipeixinxi?";
				/* 首先创建一个form表单   */
				var tempForm = document.createElement("form");    
				tempForm.id="tempForm1";
				/* 制定发送请求的方式为post   */
				tempForm.method="post";   
				/* 此为window.open的url，通过表单的action来实现   */
				tempForm.action=link;  
				/* 利用表单的target属性来绑定window.open的一些参数（如设置窗体属性的参数等） */  
				tempForm.target="_parent";
				if(arrayObj!=""){
					//var url="${ROOT}/updownload/lipeixinxi?arrayObj="+arrayObj;
					/* 创建input标签，用来设置参数   */
			        var hideInput = document.createElement("input");    
			        hideInput.type="hidden";    
			        hideInput.name= "arrayObj";  
			        hideInput.value= arrayObj;
			        /*  将input表单放到form表单里   */
			        tempForm.appendChild(hideInput); 
			        /* 将此form表单添加到页面主体body中   */
			        document.body.appendChild(tempForm); 
					/* 手动触发，提交表单   */
			        tempForm.submit();
					/* 从body中移除form表单 */  
			        document.body.removeChild(tempForm);  
				}else{
					/* var url="${ROOT}/updownload/lipeixinxi?caseId="+caseId+"&tbrxm="+tbrxm+"&bbrxm="+bbrxm+"&nd="+nd+"&bbrsfz="+bbrsfz+"&ygbh="+ygbh+"&dwmc="+dwmc;  */
					/* 创建input标签，用来设置参数   */
			        var hideInput1 = document.createElement("input");    
			        hideInput1.type="hidden";    
			        hideInput1.name= "caseId";  
			        hideInput1.value= caseId;
			        /* 创建input标签，用来设置参数   */
			        var hideInput2 = document.createElement("input");    
			        hideInput2.type="hidden";    
			        hideInput2.name= "tbrxm";  
			        hideInput2.value= tbrxm;
			        /* 创建input标签，用来设置参数   */
			        var hideInput3 = document.createElement("input");    
			        hideInput3.type="hidden";    
			        hideInput3.name= "bbrxm";  
			        hideInput3.value= bbrxm;
			        /* 创建input标签，用来设置参数   */
			        var hideInput4 = document.createElement("input");    
			        hideInput4.type="hidden";    
			        hideInput4.name= "nd";  
			        hideInput4.value= nd;
			        /* 创建input标签，用来设置参数   */
			        var hideInput5 = document.createElement("input");    
			        hideInput5.type="hidden";    
			        hideInput5.name= "bbrsfz";  
			        hideInput5.value= bbrsfz;
			        /* 创建input标签，用来设置参数   */
			        var hideInput6 = document.createElement("input");    
			        hideInput6.type="hidden";    
			        hideInput6.name= "ygbh";  
			        hideInput6.value= ygbh;
			        /* 创建input标签，用来设置参数   */
			        var hideInput7 = document.createElement("input");    
			        hideInput7.type="hidden";    
			        hideInput7.name= "dwmc";  
			        hideInput7.value= dwmc;
			        /*  将input表单放到form表单里   */
			        tempForm.appendChild(hideInput1); 
			        tempForm.appendChild(hideInput2); 
			        tempForm.appendChild(hideInput3); 
			        tempForm.appendChild(hideInput4); 
			        tempForm.appendChild(hideInput5); 
			        tempForm.appendChild(hideInput6); 
			        tempForm.appendChild(hideInput7); 
			        /* 将此form表单添加到页面主体body中   */
			        document.body.appendChild(tempForm); 
					/* 手动触发，提交表单   */
			        tempForm.submit();
					/* 从body中移除form表单 */  
			        document.body.removeChild(tempForm);  
				}
				//$("#daochu").attr("href",url);
			}
			
		} 
		
		
		
		
		
		
	/* } */
	
	
	
	// 全选
	var allDom = document.getElementsByName("allck")[0];  //找到全选框
	var numDoms = document.getElementsByName("integrateCheckbox"); //找到选项的数组

	allDom.onclick = function(){   //定义一个全选框的点击函数
	if(this.checked){       
		//判断全选框是否选中,如果返回值为true代表选中
			for(var i=0;i<numDoms.length;i++){   //使用for循环选中列表框
				numDoms[i].checked = true;
				//$('#daochu').removeAttr("disabled");
			}
	}else{     
		//如果返回值为false代表未选中
			for(var i=0;i<numDoms.length;i++){  //使用for循环取消列表框的选中状态
				numDoms[i].checked = false;
				//$('#daochu').attr('disabled',"true");
			}
		}
	}
	
	
	
	
</script>
</html>
