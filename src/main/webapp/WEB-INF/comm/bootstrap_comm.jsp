<%@page pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/html4/strict.dtd">
<!-- STYLESHEETS --><!--[if lt IE 9]><script src="${BSROOT}/js/flot/excanvas.min.js"></script><script src="${BSROOT}/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="${BSROOT}/http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="${BSROOT}/css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="${BSROOT}/css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="${BSROOT}/css/responsive.css" >
	<link rel="stylesheet" type="text/css"  href="${BSROOT}/css/simplePagination.css" >
	<link href="${BSROOT}/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="${BSROOT}/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />
	<link rel="stylesheet" type="text/css" href="${BSROOT}/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<link rel="stylesheet" type="text/css" href="${BSROOT}/js/datatables/media/css/jquery.dataTables.min.css" />
	<link rel="stylesheet" type="text/css" href="${BSROOT}/js/datatables/media/assets/css/datatables.min.css" />
	<link rel="stylesheet" type="text/css" href="${BSROOT}/js/datatables/extras/TableTools/media/css/TableTools.min.css" />
	<link rel="stylesheet" type="text/css" href="${BSROOT}/js/uniform/css/uniform.default.min.css" />
	<link rel="stylesheet" type="text/css" href="${BSROOT}/bootstrap-dist/css/bootstrap.min.css" />
	<!-- ANIMATE -->
	<link rel="stylesheet" type="text/css" href="${BSROOT}/css/animatecss/animate.min.css" />
	<script src="${BSROOT}/js/jquery/jquery-2.0.3.min.js"></script>
	<script src="${BSROOT}/js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="${BSROOT}/bootstrap-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="${BSROOT}/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script><script type="text/javascript" src="${BSROOT}/js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<script type="text/javascript" src="${BSROOT}/js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<script type="text/javascript" src="${BSROOT}/js/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="${BSROOT}/js/datatables/media/assets/js/datatables.min.js"></script>
	<script type="text/javascript" src="${BSROOT}/js/datatables/extras/TableTools/media/js/TableTools.min.js"></script>
	<script type="text/javascript" src="${BSROOT}/js/datatables/extras/TableTools/media/js/ZeroClipboard.min.js"></script>
	<script type="text/javascript" src="${BSROOT}/js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="${BSROOT}/js/jquery.simplePagination.js"></script>
	<script type="text/javascript" src="${BSROOT}/js/uniform/jquery.uniform.min.js"></script>
	<!-- BACKSTRETCH -->
	<script src="${BSROOT}/js/script.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("dynamic_table");  //Set current page
			App.init(); //Initialise plugins and elements
		});
	</script>