<%@page pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@include file="/WEB-INF/comm/_env.jsp"%>
<header class="navbar clearfix" id="header">
</style>
	<div class="container" style="background-color: black;">
		<div class="navbar-brand" style="widows: 500;width : 200px ;">
			<!-- COMPANY LOGO -->
			   <img src="${BSROOT}/img/logo-freebsd-devilx64.png"
				alt="Cloud Admin Logo" class="img-responsive" height="40px"
				width="50px"  style="height:40px;width:50px;top:4px;left:70px;">
				<span style="
				    color: white;
				    margin-left: 120px;
				    font-size: 30px;
				    /* text-shadow: 5px 5px 5px black; */
				    text-shadow: 5px 5px 4px red, 0px 0px 2px red;
				    font-family: -webkit-pictograph;
				">MC</span>			 
			<!-- /COMPANY LOGO -->
			<div class="visible-xs">
				<a href="#" class="team-status-toggle switcher btn dropdown-toggle">
					<i class="fa fa-users"></i>
				</a>
			</div>
			<div id="sidebar-collapse" class="sidebar-collapse btn">
				  <i class="fa fa-bars" data-icon1="fa fa-bars"
					data-icon2="fa fa-bars"></i>
			</div>
		</div>
		<ul class="nav navbar-nav pull-left hidden-xs" id="navbar-left">
					  <li class="dropdown" onclick="showIframe(this);" rightClick="" style="margin-top: 20px;background-color: #d2322d">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding: 5px;">
							<i class="fa"></i>
							<span class="name">首页</span>
							<i class="fa"></i>
						</a>
					</li>  
		</ul>
		<ul class="nav navbar-nav pull-right">
			<!-- BEGIN USER LOGIN DROPDOWN -->
			<li class="dropdown user" id="header-user" style="color: white;">
				<p class="navbar-text pull-right">
					<%-- 欢迎回来 <span style="font-weight: bold;"><img src="${BSROOT}/img/userx32.png">${sessionScope.yhm} </span> --%> 当前时间 ： <span id="systemDate"></span>
					<%-- <a href="#" onclick="logout();" data-toggle="modal"><img src="${BSROOT}/img/exitx32.png"></a> --%>
				</p>
			</li>
		</ul>
	</div>
	
	
	
	<!-- Edit User_Role -->
	<div class="modal fade" id="editpswRoleModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
		<input type="hidden" id="userId" name="userId" value="">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">修改密码</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
							<label class="col-sm-3 control-label">原密码：</label>
							<div class="col-sm-9">
								<input class="form-control input-sm" type="text" name="psw1" id="psw1"
									placeholder="请输入">
							</div>
						</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button class="btn btn-primary" type="button" onclick="close_role()">提交并关闭</button>
				</div>
			</div>
		</div>
	</div>
	
</header>

<script type="text/javascript">
$(document).ready(function() {
	showLocale=function(objD)
	{
		var str,colorhead,colorfoot;
		var yy = objD.getYear();
		if(yy<1900) yy = yy+1900;
		var MM = objD.getMonth()+1;
		if(MM<10) MM = '0' + MM;
		var dd = objD.getDate();
		if(dd<10) dd = '0' + dd;
		var hh = objD.getHours();
		if(hh<10) hh = '0' + hh;
		var mm = objD.getMinutes();
		if(mm<10) mm = '0' + mm;
		var ss = objD.getSeconds();
		if(ss<10) ss = '0' + ss;
		var ww = objD.getDay();
		if  ( ww==0 )  colorhead="<font color=\"white\">";
		if  ( ww > 0 && ww < 6 )  colorhead="<font color=\"white\">";
		if  ( ww==6 )  colorhead="<font color=\"white\">";
		if  (ww==0)  ww="星期日";
		if  (ww==1)  ww="星期一";
		if  (ww==2)  ww="星期二";
		if  (ww==3)  ww="星期三";
		if  (ww==4)  ww="星期四";
		if  (ww==5)  ww="星期五";
		if  (ww==6)  ww="星期六";
		colorfoot="</font>"
		str = colorhead + yy + "年" + MM + "月" + dd + "日"+" " + ww + hh + ":" + mm + ":" + ss  + colorfoot ;
		return(str);
	}
	tick=function()
	{
		var today;
		today = new Date();
		document.getElementById("systemDate").innerHTML = showLocale(today);
		window.setTimeout("tick()", 1000);
	}
	tick();
	
	
});



function showIframe(obj){
	var modular_name = $(obj).find("span").text();
	var parent_iframe_div = $(window.parent.document).find("#leftFrame").contents().find("#parent_iframe_div");
	parent_iframe_div.children("div[name='"+modular_name+"_iframe']").show().siblings().hide();
	
}

function closeCurModular(obj){
	var modular_name = $(obj).prev().text()
	var parent_iframe_div = $(window.parent.document).find("#leftFrame").contents().find("#parent_iframe_div");
	var cur_iframe_div = parent_iframe_div.children("div[name='"+modular_name+"_iframe']");
	cur_iframe_div.remove();
	var last_header_li = $(obj).parents("li.dropdown").siblings(":last");
	$(obj).parents("li.dropdown").remove();
	last_header_li.trigger("click");
	//增加选中标签背景颜色
}

$(document).ready(function(){  
	bindRightClick();
});

function bindRightClick(){
	$(document).bind("contextmenu",function(e){   
        return false;   
  });
}


/*  
(function($) {
	$.fn.extend({
		//定义鼠标右键方法，接收一个函数参数 
		"rightClick11" : function() {
			//调用这个方法后将禁止系统的右键菜单 
			$(document).bind('contextmenu', function(e) {
				return false;
			});
		}
	});
})(jQuery);
 */
/* $(document).ready(function(e){ 
	$("#navbar-left").children("li").rightClick(function(obj){
		
	}); 
});  */
</script>