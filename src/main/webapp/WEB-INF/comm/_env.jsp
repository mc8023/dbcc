<%-- 
    Document   : _env.jsp
    Created on : 2017-3-17 15:00:42
    Author     : tiansheng
--%><%@page pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ms" tagdir="/WEB-INF/tags"%>
<c:set scope="request" var="ROOT" value="http://${header.Host}${pageContext.request.contextPath}" />
<c:set scope="request" var="BSROOT" value="http://${header.Host}${pageContext.request.contextPath}/bootstrap_resource" />

<%@include file="/WEB-INF/comm/bootstrap_comm.jsp"%>
<link rel="stylesheet" type="text/css" href="${ROOT}/css/main.css" >
<link rel="stylesheet" type="text/css" href="${ROOT}/css/icono.min.css">
<script type="text/javascript" src="${ROOT}/js/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="${ROOT}/js/validation/messages_zh.js"></script>
<script type="text/javascript" src="${ROOT}/js/validation/jquery.validationEngine-zh_CN.js"></script>
<script type="text/javascript" src="${ROOT}/js/validation/jquery.validationEngine.min.js"></script>
<script type="text/javascript" src="${ROOT}/js/winning-common.js"></script>
<script type="text/JavaScript" src="${ROOT}/js/My97DatePicker/WdatePicker.js"></script> 
